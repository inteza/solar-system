#pragma strict

private var index : int = 0;
private var image : String;

function OnGUI () {
	index += 1;
	if(index < 10)
	{
		image = "Horizon_guide_app_UI_003_0000"+index.ToString();
	}
	else if(index < 100)
	{
		image = "Horizon_guide_app_UI_003_000"+index.ToString();
	}
	else image = "Horizon_guide_app_UI_003_00"+index.ToString();
	Debug.Log(Time.time);
	var texture : Texture2D = Resources.Load(image) as Texture2D;
	GUI.DrawTexture(Rect(0,0,Screen.width,Screen.height),texture, ScaleMode.ScaleToFit);
}

function Start () {

}

function Update () {
	
}