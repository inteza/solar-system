using UnityEngine;
using System.Collections;
using System.Xml; 
using System.Xml.Serialization;
using System.IO; 
using System.Text; 

public class ShortInfoPlanet {
	
	[XmlAttribute("id")]
	public int id;
	
	public Vector3 p;
	public Vector3 r;
	public int s;
	public int o;
	public int oh;
}
