using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml; 
using System.Xml.Serialization;
using System.IO; 
using System.Text; 

[XmlRoot("SSI"), System.Serializable]
public class SystemShortInfo {
	
	[XmlElement("SIC")]
	public ShortInfoCamera cameraShortInfo;
	
	[XmlElement("SIP")]
	public List<ShortInfoPlanet> planetsShortInfo;
	
	[XmlIgnoreAttribute]
	private static SystemShortInfo instance;
	
	public static SystemShortInfo Instance{
		get{
			if (instance == null) instance = new SystemShortInfo();
			return instance;
		}
	}
	
	private SystemShortInfo(){
		
	}
	
	
	
	public static SystemShortInfo ParseText(string text){
		
		XmlSerializer serializer = new XmlSerializer(typeof(SystemShortInfo));
		StringReader stringReader = new StringReader(text);
		SystemShortInfo systemShortInfo = serializer.Deserialize(stringReader) as SystemShortInfo;
		stringReader.Close();
		return systemShortInfo;
	}
}
