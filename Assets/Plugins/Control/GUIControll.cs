using UnityEngine;
using System.Collections;
using AssemblyCSharp;

public class GUIControll : MonoBehaviour {
	
	UDKServer server;
	Scene scene;
	
	Vector2 scrollPosition;
	
	// Use this for initialization
	void Start () {
		server = GetComponent<UDKServer>();
		scene = GetComponent<Scene>();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Escape)){
			Application.Quit();
		}
	}
	
	private void PlanetSelect(Commands.SkyObjectID objectID){
		server.sendCommand((int)objectID);
	}
	
	void OnGUI(){
		
		scrollPosition = GUI.BeginScrollView(new Rect(0, 0, Screen.width, Screen.height), scrollPosition, new Rect(0, 0, Screen.width, 1000), true, true);
		
		GUIConnect();
		GUIPlanetList();
		GUIOrbits();
		GUIZoom();
		GUITouches();
		
		GUI.EndScrollView();
	}
	
	private void GUIZoom(){
		
		GUI.BeginGroup(new Rect(10, 350, Screen.width, 100));
		if (GUI.Button(new Rect(10, 10, 150, 30), "Zoom In")){
			server.sendCommand((int)Commands.ClientMessages.ZoomIn);
		}
		else if (GUI.Button(new Rect(170, 10, 150, 30), "Zoom Out")){
			server.sendCommand((int)Commands.ClientMessages.ZoomOut);
		}
		GUI.EndGroup();
	}
	
	private void GUITouches(){
		
		GUI.BeginGroup(new Rect(10, 450, Screen.width, 300));
		
		if (GUI.Button(new Rect(10, 10, 50, 50), "")){
			server.sendCommand((int)Commands.ClientMessages.TouchLeftUp);
		}
		else if (GUI.Button(new Rect(60, 10, 50, 50), "^")){
			server.sendCommand((int)Commands.ClientMessages.TouchUp);
		}
		else if (GUI.Button(new Rect(110, 10, 50, 50), "")){
			server.sendCommand((int)Commands.ClientMessages.TouchRightUp);
		}
		
		else if (GUI.Button(new Rect(10, 60, 50, 50), "<")){
			server.sendCommand((int)Commands.ClientMessages.TouchLeft);
		}
		else if (GUI.Button(new Rect(110, 60, 50, 50), ">")){
			server.sendCommand((int)Commands.ClientMessages.TouchRight);
		}
		
		else if (GUI.Button(new Rect(10, 110, 50, 50), "")){
			server.sendCommand((int)Commands.ClientMessages.TouchLeftDown);
		}
		else if (GUI.Button(new Rect(60, 110, 50, 50), "")){
			server.sendCommand((int)Commands.ClientMessages.TouchDown);
		}
		else if (GUI.Button(new Rect(110, 110, 50, 50), "")){
			server.sendCommand((int)Commands.ClientMessages.TouchRightDown);
		}
		
		GUI.EndGroup();
	}
	
	private void GUIOrbits(){
		
		GUI.BeginGroup(new Rect(10, 250, Screen.width, 100));
		
		
		if (GUI.Button(new Rect(10, 10, 150, 30), "Show Orbit")){
			server.sendCommand((int)Commands.ClientMessages.ShowOrbit);
		}
		else if (GUI.Button(new Rect(170, 10, 150, 30), "Show Orbit Height")){
			server.sendCommand((int)Commands.ClientMessages.ShowOrbitHeight);
		}
		else if (GUI.Button(new Rect(10, 50, 150, 30), "Hide Orbit")){
			server.sendCommand((int)Commands.ClientMessages.HideOrbit);
		}
		else if (GUI.Button(new Rect(170, 50, 150, 30), "Hide Orbit Height")){
			server.sendCommand((int)Commands.ClientMessages.HideOrbitHeight);
		}
		
		GUI.EndGroup();
	}
	
	private void GUIPlanetList(){
		
		
		GUI.BeginGroup(new Rect(10, 100, Screen.width, 200));
		
		if (GUI.Button(new Rect(10, 10, 100, 30), "Earth")){
			PlanetSelect(Commands.SkyObjectID.PlanetEarth);
		}
		else if (GUI.Button(new Rect(120, 10, 100, 30), "Mercury")){
			PlanetSelect(Commands.SkyObjectID.PlanetMercury);
		}
		else if (GUI.Button(new Rect(230, 10, 100, 30), "Venus")){
			PlanetSelect(Commands.SkyObjectID.PlanetVenus);
		}
		
		else if (GUI.Button(new Rect(10, 50, 100, 30), "Saturn")){
			PlanetSelect(Commands.SkyObjectID.PlanetSaturn);
		}
		else if (GUI.Button(new Rect(120, 50, 100, 30), "Mars")){
			PlanetSelect(Commands.SkyObjectID.PlanetMars);
		}
		else if (GUI.Button(new Rect(230, 50, 100, 30), "Jupiter")){
			PlanetSelect(Commands.SkyObjectID.PlanetJupiter);
		}
		
		else if (GUI.Button(new Rect(10, 90, 100, 30), "Neptune")){
			PlanetSelect(Commands.SkyObjectID.PlanetNeptune);
		}
		else if (GUI.Button(new Rect(120, 90, 100, 30), "Uranus")){
			PlanetSelect(Commands.SkyObjectID.PlanetUranus);
		}
		else if (GUI.Button(new Rect(230, 90, 100, 30), "Sun")){
			PlanetSelect(Commands.SkyObjectID.Sun);
		}
		GUI.EndGroup();
	}
	
	private void GUIConnect(){
		GUI.BeginGroup(new Rect(10, 10, Screen.width, 100));
		server.ip = GUI.TextArea(new Rect(10, 10, 100, 30), server.ip);
	 	server.port = int.Parse(GUI.TextArea(new Rect(120, 10, 100, 30), server.port.ToString()));
		if (GUI.Button(new Rect(240, 10, 100, 30), "Connect")){
			server.Login();
		}
		string loginStatus = "";
		if (server.logined)	loginStatus = "Ok";
		else loginStatus = "Error";
		GUI.Label(new Rect(240, 40, 100, 30), loginStatus);
		GUI.EndGroup();
	}
}
