using System;
using UnityEngine;
using System.Collections;
using Thrift;
using Thrift.Transport;
using Thrift.Protocol;
using Thrift.Server;

public class UDKServer : MonoBehaviour {
	
	UDKService.Client client = null;
	const int succeeded = 123456;
	
	public string ip = "192.168.0.70";
	public int port = 9092;
	
	public bool logined;
	
	// Use this for initialization
	void Start () {
//		this.Login();
		
//		StartCoroutine(StartUpdate());
	}
	
	float lastUpdateTime = Time.time;
	
	// Update is called once per frame
	void Update () {
	
//		if (Time.time - lastUpdateTime >= 1f){
//			lastUpdateTime = Time.time;
//			
			ReadState();
//		}
		
	}
	
	void OnGUI()
	{
//		if( GUI.Button( new Rect( 10, 10, 100, 40 ), "Update position" ) )
//		{
//			this.ReadState();
//		}
	}
	
	private IEnumerator StartUpdate(){
		
		while(true){
			yield return new WaitForSeconds(1f);
			
			ReadState();
			
		}
	}
	
	void ReadState()
	{
		if( client != null )
		{
			string state = "";
			try{
				state = client.ReadState();
			}
			catch(TApplicationException e){
				Debug.LogError("Catched " + e.Message);
			}
			
			if (state == ""){
				return;
			}
			// parse state
			SystemShortInfo ssi = SystemShortInfo.ParseText(state);
			foreach(ShortInfoPlanet sip in ssi.planetsShortInfo)
				Scene.Instance.UpdatePlanetPosition(sip);
			
			Scene.Instance.UpdateCameraPosition(ssi.cameraShortInfo);
			//
			
			ServerController.Instance.ServerInfoCome(ssi);
		}
		
		return ;
	}
	
	public bool Login()
	{		
		try{
			TTransport transport = new TSocket(ip, port);
			TProtocol protocol = new TBinaryProtocol(transport);
			client = new UDKService.Client(protocol);
			transport.Open();
			int val = client.Login(123, 1);
			Debug.Log( val );
			logined = (val == succeeded);
			return logined;
		}catch(TTransportException ex)
		{
			logined = false;
			return false;
		}
	}
	
	public bool sendCommand( int commandId )
	{
		if( client != null )
		{
			try
			{
				client.send_SetBoolProperty(commandId, true);	
				return true;
			}catch(TTransportException ex)
			{
				return false;
			}
		}else 
			return false;
	}
}
