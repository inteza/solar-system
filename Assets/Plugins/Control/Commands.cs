using System;

namespace AssemblyCSharp
{
	
	public static class Commands
	{
		public enum SkyObjectID{
			Sun	= 1001,
			PlanetEarth	= 1002,
			PlanetMercury = 1003,
			PlanetVenus	= 1004,
			PlanetSaturn = 1005,
			PlanetMars = 1006,
			PlanetJupiter = 1007,
			PlanetNeptune = 1008,
			PlanetUranus = 1009,
			PlanetPluton = 10045,
			SateliteMoon = 1010,
			SateliteEvropa = 1011,
			SateliteCalisto = 1012,
			SateliteGanimede = 1013,
			SateliteIO = 1014,
			SateliteDeimos = 1015,
			SatelitePhobos = 1016,
			SateliteMimos = 1017,
			SateliteEnceladus = 1018,
			SateliteTehys = 1019,
			SateliteDione = 1020,
			SateliteRhea = 1021,
			SateliteTitan = 1022,
			NoName = 1023
		}
		
		public enum ClientMessages{
			
			ShowOrbit = 3001,
			HideOrbit = 3002,
			ShowOrbitHeight = 3003,
			HideOrbitHeight = 3004,
			
			ZoomIn = 3201,
			ZoomOut = 3202,
			
			TouchUp = 3101,
			TouchRightUp = 3102,
			TouchRight = 3103,
			TouchRightDown = 3104,
			TouchDown = 3105,
			TouchLeftDown = 3106,
			TouchLeft = 3107,
			TouchLeftUp = 3108,
			
		}
	}
	
	
}

