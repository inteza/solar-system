
using UnityEngine;
using System.Collections;
using AssemblyCSharp;

public class Scene : MonoBehaviour
{
	Hashtable cerestials = new Hashtable();
	
	private static Scene instance;
	
	public static Scene Instance{
		get{
			if (instance == null) instance = new Scene();
			return instance;
		}
	}
	
	void Awake()
	{
		Scene.instance = this;
		
		RegisterObject(GameObject.Find("mercury"), (int)Commands.SkyObjectID.PlanetMercury);
		RegisterObject(GameObject.Find("venus"),   (int)Commands.SkyObjectID.PlanetVenus);
		RegisterObject(GameObject.Find("Planet Earth"),   (int)Commands.SkyObjectID.PlanetEarth);
		RegisterObject(GameObject.Find("mars"),    (int)Commands.SkyObjectID.PlanetMars);
		RegisterObject(GameObject.Find("saturn"),  (int)Commands.SkyObjectID.PlanetSaturn);
		RegisterObject(GameObject.Find("jupiter"), (int)Commands.SkyObjectID.PlanetJupiter);	
		RegisterObject(GameObject.Find("neptune"), (int)Commands.SkyObjectID.PlanetNeptune);	
		RegisterObject(GameObject.Find("pluto"), (int)Commands.SkyObjectID.PlanetPluton);	
		RegisterObject(GameObject.Find("uranus"), (int)Commands.SkyObjectID.PlanetUranus);	
	}
		
	
	public Scene ()
	{
			
	}
	
	public void RegisterObject( GameObject obj, int id)
	{
		cerestials.Add(id, obj);
	}
	
	public void UpdatePlanetPosition( ShortInfoPlanet data )
	{
		if( cerestials.ContainsKey(data.id) )
		{
			GameObject obj = cerestials[data.id] as GameObject;
			obj.transform.position = data.p;
			obj.transform.rotation = Quaternion.Euler(data.r);
		}
	}
	
	public void UpdateCameraPosition( ShortInfoCamera data )
	{
		Camera.mainCamera.transform.position = data.p;
		Camera.mainCamera.transform.rotation = Quaternion.LookRotation(data.d);
	}
}

