using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using AssemblyCSharp;

public class ServerController : MonoBehaviour {
	
	private UDKServer server;
	private Commands.SkyObjectID selectedPlanetId = Commands.SkyObjectID.NoName;
	private Hashtable planetsInfo;
	
	private static ServerController instance;
	
	public static ServerController Instance{
		get{return instance;}
	}
	
	public Commands.SkyObjectID SelectedPlanet{
		get{return selectedPlanetId;}
	}
	
	void Awake(){
		instance = this;
		planetsInfo = new Hashtable();
		server = GetComponent<UDKServer>();
	}
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void SelectPlanet(Commands.SkyObjectID skyObjectID){
		if (skyObjectID == selectedPlanetId){
			return;
		}
		server.sendCommand((int)skyObjectID);
	}
	
	public void ZoomIn(){
		server.sendCommand((int)Commands.ClientMessages.ZoomIn);
	}
	
	public void ZoomOut(){
		server.sendCommand((int)Commands.ClientMessages.ZoomOut);
	}
	
	public void OnTouch(Commands.ClientMessages touchMessage){
		server.sendCommand((int)touchMessage);
	}
	
	public void ShowOrbit(){
		if (selectedPlanetId == Commands.SkyObjectID.NoName){
			return;
		}
		if (IsOrbitShowed(selectedPlanetId)){
			return;
		}
		server.sendCommand((int)Commands.ClientMessages.ShowOrbit);
	}
	
	public bool IsOrbitShowed(Commands.SkyObjectID skyObjectID){
		ShortInfoPlanet sip = planetsInfo[(int)selectedPlanetId] as ShortInfoPlanet;
		if (sip == null) {return false;}
		else {return sip.o == 1;};
	}
	
	public bool IsOrbitShowed(){
		if (selectedPlanetId == Commands.SkyObjectID.NoName){
			return false;
		}
		else return IsOrbitShowed(selectedPlanetId);
	}
	
	public void HideOrbit(){
		if (selectedPlanetId == Commands.SkyObjectID.NoName){
			return;
		}
		if (IsOrbitShowed(selectedPlanetId) == false){
			return;
		}
		server.sendCommand((int)Commands.ClientMessages.HideOrbit);
	}
	
	public void ShowOrbitHeight(){
		if (selectedPlanetId == Commands.SkyObjectID.NoName){
			return;
		}
		if (IsOrbitHeightShowed(selectedPlanetId)){
			return;
		}
		server.sendCommand((int)Commands.ClientMessages.ShowOrbitHeight);
	}
	
	public bool IsOrbitHeightShowed(Commands.SkyObjectID skyObjectID){
		ShortInfoPlanet sip = planetsInfo[(int)selectedPlanetId] as ShortInfoPlanet;
		if (sip == null) return false;
		else return sip.oh == 1;
	}
	
	public bool IsOrbitHeightShowed(){
		if (selectedPlanetId == Commands.SkyObjectID.NoName){
			return false;
		}
		else return IsOrbitHeightShowed(selectedPlanetId);
	}
	
	public void HideOrbitHeight(){
		if (selectedPlanetId == Commands.SkyObjectID.NoName){
			return;
		}
		if (IsOrbitHeightShowed(selectedPlanetId) == false){
			return;
		}
		server.sendCommand((int)Commands.ClientMessages.HideOrbitHeight);
	}
	
	public void ServerInfoCome(SystemShortInfo ssi){
		
		foreach(ShortInfoPlanet sip in ssi.planetsShortInfo){
			if (planetsInfo.ContainsKey(sip.id)){
				planetsInfo[sip.id] = sip;
			}
			else{
				planetsInfo.Add(sip.id, sip);
			}
			
			if (sip.s == 1){
				this.selectedPlanetId = (Commands.SkyObjectID)sip.id;
				Debug.Log("Selected planet: " + this.selectedPlanetId);
			}
		}
	}
}
