using UnityEngine;
using System.Collections;
using System;

public class Cerestial : MonoBehaviour {
	
	public Transform rootPlanet;
	// earth 60 190,03
	public double SemimajorAxis = 1; //eD/(1-ee) ; 		// semimajor axis
    public double TimeOfPerihelion=2009.920548;		// start time T0
     		//double edistp=0.98328978;
    public double Eccentricity=0.01671022;			// + e
    public double ArgumentPerihelion=102.93768193;  // + w
    public double LongitudeAscendingNode=0+0;			// +
    public double Inclination=0+0;				// +
	public double AxisScale=200;				// +
	
	public float axialTilt;
	
	private Cerestial rootCerestial;
	
//	public float startTime = 2010;
	
	void Awake(){

		rootCerestial = rootPlanet.gameObject.GetComponent<Cerestial>();
	}
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
		this.transform.position = GetPositionInTime(TimeController.Instance.CurrentTime);
		this.transform.rotation = Quaternion.AngleAxis(axialTilt, Vector3.forward);
	}
	
	public Vector3 GetPositionInTime(double time){
		Vector3 direction = calcPos(time);
		
		if (rootCerestial != null){
			float angle = rootCerestial.axialTilt;
			
		 	direction = Quaternion.AngleAxis(angle, Vector3.forward) * direction;
		}
		
		return rootPlanet.transform.position + direction.normalized * (float)AxisScale;
	}
	
	double lf( double val, double div )
	{
	    if(val == 0 ) return val;
	    
	    int n = (int)(val / div);
	    return val - div * n;
	}

	// return the integer part of a number
	// borrowed from Stephen R. Schmitt
	int abs_floor(double xflor )
	{
	    int rrr;
	    if (xflor >= 0.0) 
			rrr = (int)Math.Floor(xflor);
	    else          
			rrr = (int)Math.Ceiling(xflor);
	    return rrr;
	}

		double mod2pi(double degrees )
		{
		    double bbb = degrees/(2 * Math.PI);
		    double aaa = (2 * Math.PI)* (bbb - abs_floor(bbb));
		    if (aaa < 0) aaa = (2 * Math.PI) + aaa;
		    return aaa;
		}
		
	
	public double Period(){
		double ea=SemimajorAxis;
		double ePP=ea*ea*ea ;
		return Math.Sqrt(ePP);
	}
	
	public Vector3 calcPos(double currentTime)
	{
		double ea=SemimajorAxis;
     	double etimep=TimeOfPerihelion;	
     	double eecc=Eccentricity;
    	double eargpdeg=ArgumentPerihelion;
    	double elongndeg=LongitudeAscendingNode;
    	double einc=Inclination;	
		
		double pi = Math.PI;
	
    		double deg=(180/pi);
    
//			startTime += deltaTime * timeScale;
		
//			double time = ;//(document.forms["calc"].currentTime.value)*1 ; //read this stuff
    
    		
		    //solve for semimajor axis a
		    //double eD=edistp ;
		    double ee=eecc ;
		    //double ea=1;//eD/(1-ee) ;
		//    ea = 1;
//		    Debug.Log(string.Format("ea = {0}", ea));
		    
		    //solve for period P
		    double ePP=ea*ea*ea ;
		    double eP = Math.Sqrt(ePP);
//		    Debug.Log(string.Format("eP = {0}", eP));
		    
    
		    //solve for M
		    double et = currentTime;
		    double eQ = etimep ; 
			double eL = eQ; //last perihelion
			while (eL >= et)
			{
		        eL = eL - eP;
		    }
		    
		    double eM=2*pi*(lf(Mathf.Abs((float)(et-eL)), eP)) / eP ;
//		    Debug.Log(string.Format("eM = {0}", eM*deg));
//		    Debug.Log(string.Format("eL = {0}", et-(lf(Mathf.Abs((float)(et-eL)), eP))));
		    
		    //solve for E (eccentric anomaly) using multiple iterations
		    // arguments:
		    // ec=eccentricity, m=mean anomaly,
		    // dp=number of decimal places
		    double K=pi/180.0;
		    int maxIter=200, i=0;
		    double delta = Mathf.Pow(10,-30);
		    double eE, eF;
		    double em = eM;
		    double eec = ee;
		    //        m=2.0*pi*(m-Math.floor(m));
		    if ( eec < 0.8) eE=em; else eE=180.0;//>
		    eF = eE - eec * Math.Sin(em) - em;
		    while ((Mathf.Abs((float)eF)>delta) && (i<maxIter)) {//> to cut out the blue html
		        eE = eE - eF / (1.0 - eec * Math.Cos(eE));
		        eF = eE - eec * Math.Sin(eE) - em;
		        i = i + 1;
		    }
		    //done
//		    Debug.Log(string.Format("eE = {0}", eE*deg));
		    
		    //using E and e solve for phi
			double ephi=mod2pi(2*Math.Atan(Math.Sqrt((1 + ee)/(1 - ee))*Math.Tan(0.5*eE)));
		    
//		    Debug.Log(string.Format("ephi = {0}", ephi*deg));
		    
		    //using phi solve for r radial distance
			double er=ea*(1-ee*ee)/(1+ee*Math.Cos(ephi));
//		    Debug.Log(string.Format("er = {0}", er));
		    
		    //define orbital parameters
		
		    double eomega=elongndeg/deg ;
		    double ew=(eargpdeg)/deg ; //this may be only argperihelion in deg
		    double eincl=einc/deg ;
//		    Debug.Log(string.Format("omega = {0}", eomega*deg));
//		    Debug.Log(string.Format("w = {0}", ew*deg));
//		    Debug.Log(string.Format("incl = {0}",  eincl*deg));
		    
		    //calculate heliocentric ecliptical coordinates
		    double xec = er*(Math.Cos(eomega)*Math.Cos(ew+ephi)-Math.Sin(eomega)*Math.Sin(ew+ephi)*Math.Cos(eincl));
		    //Debug.Log(string.Format("x = {0}", xec)); 
		    double zec = er*(Math.Sin(eomega)*Math.Cos(ew+ephi)+Math.Cos(eomega)*Math.Sin(ew+ephi)*Math.Cos(eincl));
		    //Debug.Log(string.Format("y = {0}", yec));
		    double yec = er * Math.Sin(ew+ephi) * Math.Sin(eincl);
		    //Debug.Log(string.Format("z = {0}", zec));
		
			//this.transform.lo
			
			Vector3 direction = new Vector3((float)xec, (float)yec, (float)zec);
			
			return direction;
	}
}
