using UnityEngine;
using System.Collections;
using System;

public class TimeController : MonoBehaviour {
	
	private double currentTime;
	private float currentTimeScale;
	private float orbitSize = 0.5f;
	private float orbitHeightSize = 1f;
	
	
	private static TimeController instance;
	
	public static TimeController Instance{
		get{
			return instance;
		}
	}
	
	public float TimeScale{
		get{return currentTimeScale;}
	}
	
	public double CurrentTime{
		get{return currentTime;}
	}
	
	public float OrbitSize{
		get{return orbitSize;}
	}
	
	public float OrbitHeightSize{
		get{return orbitHeightSize;}
	}
	
	void Awake(){
		instance = this;
	}
	
	// Use this for initialization
	void Start () {
//		currentTime = Convert(DateTime.Now);
	}
	
	// Update is called once per frame
	void Update () {
		currentTime += Time.deltaTime * currentTimeScale;
	}
	
	void OnGUI(){
		
	//	if (!CameraController.Instance.ShowGUI){
	//		return;
	//	}
		
	//	GUI.BeginGroup(new Rect(Screen.width/2, Screen.height/2 + 50 , 200, 200));
	//	GUI.Box(new Rect(10, 10, 180, 180), "Time Control");
	//	GUI.Label(new Rect(30, 30, 160, 30), "Time Scale: " + currentTimeScale.ToString("0.00"));
	//	currentTimeScale = GUI.HorizontalSlider(new Rect(20, 50, 160, 30), currentTimeScale, -2f, 2f);
	//	TimeSpan timeSpan = TimeSpan.FromDays(currentTime * 365f);
	//	DateTime dateTime = new DateTime(timeSpan.Ticks);
	//	GUI.Label(new Rect(30, 60, 160, 60), dateTime.ToLongDateString());
		
	//	GUI.Label(new Rect(30, 80, 160, 30), "Orbit Size:");
	//	orbitSize = GUI.HorizontalSlider(new Rect(20, 100, 160, 30), orbitSize, 0, 1);
		
	//	GUI.Label(new Rect(30, 120, 160, 30), "Orbit Height Size:");
	//	orbitHeightSize = GUI.HorizontalSlider(new Rect(20, 140, 160, 30), orbitHeightSize, 0, 1);
	//	GUI.EndGroup();
	//}
	
	//public static double Convert(DateTime dateTime){
	//	return (double)(dateTime.Year);// * 365f + dateTime.DayOfYear;
	}
}
