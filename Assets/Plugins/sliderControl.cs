using UnityEngine;
using System.Collections;

public class sliderControl : MonoBehaviour {
	
	public float sliderValue = 1.0f;
	public float currentPeriod;
	public GUISkin skin;
	
	// Use this for initialization
	void OnGUI () {
		GUI.skin = skin;
		double screenScale = Screen.height/768.0;
		double screenWidth = Screen.width;
		float x = (float)screenWidth - 100*(float)screenScale - 20;
		float width = 100*(float)screenScale;
		float height = 30*(float)screenScale;
		Rect newRect = new Rect(x, 10, width, height);
		sliderValue = GUI.HorizontalSlider (newRect, sliderValue, 0.0f, 10.0f);
	}
	
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
