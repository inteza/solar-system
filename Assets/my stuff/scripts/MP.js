#pragma strict

@script RequireComponent(AudioSource);
@script RequireComponent(AudioListener);

	//public var Movie : MovieTexture;
	public var playMovie : System.Boolean;
	public var videoName : String;
	private var timer : float;

function OnGUI () {
		if(playMovie)
		{
			playMovie = false;
			Handheld.PlayFullScreenMovie (videoName, Color.black, FullScreenMovieControlMode.Full,FullScreenMovieScalingMode.Fill);
		}
}

function Update () {
		if(playMovie)
			timer += Time.deltaTime;
}