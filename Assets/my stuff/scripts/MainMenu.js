﻿#pragma strict

private var guiRatio : float;  
//the screen width  
private var sWidth : float;  
//create a scale Vector3 with the above ratio  
private var GUIsF : Vector3;  
private var sphere : GameObject;

public var material : Material;

function OnGUI ()
{
	GUI.matrix = Matrix4x4.TRS(new Vector3(GUIsF.x,Screen.height - 89*GUIsF.y,0),Quaternion.identity,GUIsF);  
        //draw GUI on the top left  
        GUI.Button(new Rect(10,-10,258,89),"label left"); 

 	GUI.matrix = Matrix4x4.TRS(new Vector3(Screen.width/2 - 45*GUIsF.x,GUIsF.y,0),Quaternion.identity,GUIsF);  
        //draw GUI on the top left  
        GUI.Button(new Rect(0,10,60,60),"label 1"); 
	GUI.matrix = Matrix4x4.TRS(new Vector3(Screen.width/2 - 135*GUIsF.x,GUIsF.y,0),Quaternion.identity,GUIsF);  
        //draw GUI on the top left  
    	GUI.Button(new Rect(0,10,60,60),"label 2"); 
    GUI.matrix = Matrix4x4.TRS(new Vector3(Screen.width/2 + 45*GUIsF.x,GUIsF.y,0),Quaternion.identity,GUIsF);  
        //draw GUI on the top left  
    	GUI.Button(new Rect(0,10,60,60),"label 3"); 
    GUI.matrix = Matrix4x4.TRS(new Vector3(Screen.width/2 + 135*GUIsF.x,GUIsF.y,0),Quaternion.identity,GUIsF);  
        //draw GUI on the top left  
        if(GUI.Button(new Rect(0,10,60,60),"label 4"))
        {	
        	Application.LoadLevel(1);
        }
        
    GUI.matrix = Matrix4x4.TRS(new Vector3(Screen.width/2 + 300*GUIsF.x,Screen.height/2 - 200*GUIsF.y,0),Quaternion.identity,GUIsF);  
        //draw GUI on the top left  
        GUI.Button(new Rect(0,10,100,100),"label 1");
    GUI.matrix = Matrix4x4.TRS(new Vector3(Screen.width/2 + 325*GUIsF.x,Screen.height/2 - 50*GUIsF.y,0),Quaternion.identity,GUIsF);  
        //draw GUI on the top left  
        GUI.Button(new Rect(0,10,100,100),"label 1");       
    GUI.matrix = Matrix4x4.TRS(new Vector3(Screen.width/2 + 300*GUIsF.x,Screen.height/2 + 100*GUIsF.y,0),Quaternion.identity,GUIsF);  
        //draw GUI on the top left  
        GUI.Button(new Rect(0,10,100,100),"label 1");  
    GUI.matrix = Matrix4x4.TRS(new Vector3(Screen.width/2 + 250*GUIsF.x,Screen.height/2 + 250*GUIsF.y,0),Quaternion.identity,GUIsF);  
        //draw GUI on the top left  
        if(GUI.Button(new Rect(0,10,100,100),"label 1"))
        {	
        	Application.LoadLevel(1);
        }
        
	GUI.matrix = Matrix4x4.TRS(new Vector3(Screen.width - 258*GUIsF.x,Screen.height - 89*GUIsF.y,0),Quaternion.identity,GUIsF);  
        //draw GUI on the bottom right  
        GUI.Button(new Rect(-10,-10,258,89),"label right");
}

function Start () 
{
//get the screen's width  
        sWidth = Screen.width;  
        //calculate the scale ratio  
        guiRatio = sWidth/1920;  
        //create a scale Vector3 with the above ratio  
        GUIsF = new Vector3(guiRatio,guiRatio,1);
        sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        sphere.transform.position = new Vector3(0,0,4);
        sphere.transform.localScale = new Vector3(4.0f, 4.0f, 4.0f);
        sphere.renderer.material = material;
}

function Update () 
{
	sphere.transform.Rotate(Vector3.down * Time.deltaTime * 50);
}