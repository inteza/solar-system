﻿#pragma strict

var touchCount: int = 0;
var lastTouch: Touch;
function OnGUI()
{
    if(touchCount > 0) //if there is a touch print the details
    {
       GUI.Label(Rect(10,10,200,50),"Position: " + lastTouch.position);
       GUI.Label(Rect(10,30,200,50),"Finger Id: " + lastTouch.fingerId);
       GUI.Label(Rect(10,50,200,50),"Position Change: " + lastTouch.deltaPosition);
       GUI.Label(Rect(10,70,200,50),"Time Passed: " + lastTouch.deltaTime);
       GUI.Label(Rect(10,90,200,50),"Tap Count: " + lastTouch.tapCount);
       GUI.Label(Rect(10,110,200,50),"Phase: " + lastTouch.phase);
    }
}

function Start () {

}

function Update () {
//there has been a touch 
    touchCount = Input.touchCount;
    if(touchCount > 0) 
    {
       lastTouch = Input.GetTouch(0); 
    }
}