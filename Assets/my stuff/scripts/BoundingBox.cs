using UnityEngine;
using System.Collections;

public class BoundingBox : MonoBehaviour
{

        public GameObject target;
        public Camera cam;
        public Texture tex;
        public float defaultBoundsHalfSize = 15.0f;

        void OnGUI()
        {
                if (target == null) return;

                Vector3 center, extends;
                if (target.renderer != null)
                {
                        center = target.renderer.bounds.center;
                        extends = target.renderer.bounds.extents;
                }
                else
                {
                        center = target.transform.position;
                        extends = new Vector3(defaultBoundsHalfSize, defaultBoundsHalfSize, 0);
                }

                if (cam == null) cam = Camera.main;

                Vector3[] vec = new Vector3[8];

                vec[0] = cam.WorldToScreenPoint(new Vector3(center.x - extends.x, center.y + extends.y, center.z + extends.z));
                vec[1] = cam.WorldToScreenPoint(new Vector3(center.x + extends.x, center.y + extends.y, center.z + extends.z));
                vec[2] = cam.WorldToScreenPoint(new Vector3(center.x - extends.x, center.y - extends.y, center.z + extends.z));
                vec[3] = cam.WorldToScreenPoint(new Vector3(center.x + extends.x, center.y - extends.y, center.z + extends.z));

                vec[4] = cam.WorldToScreenPoint(new Vector3(center.x - extends.x, center.y + extends.y, center.z - extends.z));
                vec[5] = cam.WorldToScreenPoint(new Vector3(center.x + extends.x, center.y + extends.y, center.z - extends.z));
                vec[6] = cam.WorldToScreenPoint(new Vector3(center.x - extends.x, center.y - extends.y, center.z - extends.z));
                vec[7] = cam.WorldToScreenPoint(new Vector3(center.x + extends.x, center.y - extends.y, center.z - extends.z));

                float xMinf, xMaxf, yMinf, yMaxf;
                xMaxf = yMaxf = 0;
                xMinf = yMinf = 10000;

                for (int i = 0; i < 8; i++)
                {
                        if (vec[i].x < xMinf) xMinf = vec[i].x;
                        if (vec[i].y < yMinf) yMinf = vec[i].y;
                        if (vec[i].x > xMaxf) xMaxf = vec[i].x;
                        if (vec[i].y > yMaxf) yMaxf = vec[i].y;
                }

                Rect boxRect = new Rect(xMinf, Screen.height - yMinf - (yMaxf - yMinf), xMaxf - xMinf, yMaxf - yMinf);
                if (tex != null)
                {
                        GUI.DrawTexture(boxRect, tex, ScaleMode.StretchToFill);
                }
                else
                {
                        GUI.Box(boxRect, "");
                }
        }
}