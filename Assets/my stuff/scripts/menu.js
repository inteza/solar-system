#pragma strict

private var tmpDistance : float;

private var texture : Texture2D;
private var index : int = 0;
private var indexForMenu : int = 0;
private var effect : System.Boolean;
//old
public var skin : GUISkin;
public var btn1 : GUISkin;
public var btn2 : GUISkin;
public var btn3 : GUISkin;
public var btn4 : GUISkin;
public var skinSecond : GUISkin;
private var sl : sliderControl;

private var names = ["venus","mercury","earth","mars","jupiter","saturn","uranus","neptune","pluto","moon","demios","phobos","calisto","europa","ganymede","Io","diona","enceladus","mimas","rhea","tehys","titan"];

var speed : float = 0.0;
private var s : float = 3;
private var screenRightSide : float = Screen.width;
private var menuPoint : float = Screen.width/2;

private var guiRatio : float;
//the screen width
private var sWidth : float;
//create a scale Vector3 with the above ratio
private var GUIsF : Vector3;

private var actualScale : Vector3;

private var mars : GameObject;
private var sun : GameObject;
private var earth : GameObject;
private var mercury : GameObject;
private var jupiter : GameObject;
private var neptune : GameObject;
private var pluto : GameObject;
private var saturn : GameObject;
private var uranus : GameObject;
private var venus : GameObject;

private var menu : System.Boolean;
private var marsKey : System.Boolean;
private var sunKey : System.Boolean;
private var earthKey : System.Boolean;
private var mercuryKey : System.Boolean;
private var jupiterKey : System.Boolean;
private var neptuneKey : System.Boolean;
private var plutoKey : System.Boolean;
private var saturnKey : System.Boolean;
private var uranusKey : System.Boolean;
private var venusKey : System.Boolean;

//Satelites
private var moonKey : System.Boolean;
private var calistoKey : System.Boolean;
private var europaKey : System.Boolean;
private var ganymedeKey : System.Boolean;
private var loKey : System.Boolean;
private var demiosKey : System.Boolean;
private var phobosKey : System.Boolean;
private var dionaKey : System.Boolean;
private var enceladusKey : System.Boolean;
private var mimasKey : System.Boolean;
private var rheaKey : System.Boolean;
private var tehysKey : System.Boolean;
private var titanKey : System.Boolean;

private var moon : GameObject;
private var calisto : GameObject;
private var europa : GameObject;
private var ganymede : GameObject;
private var lo : GameObject;
private var demios : GameObject;
private var phobos : GameObject;
private var diona : GameObject;
private var enceladus : GameObject;
private var mimas : GameObject;
private var rhea : GameObject;
private var tehys : GameObject;
private var titan : GameObject;

private var animation : Animation;
private var sf : SmoothFollow;

private var drawGUIforPlanet : System.Boolean;
private var info : System.Boolean;
private var images : System.Boolean;
private var satelites : System.Boolean;

private var showingDetails : System.Boolean;
private var moveBack : System.Boolean;

var joy : GameObject;

private var target : Transform;
var distance = 8.0;

var xSpeed = 250.0;
var ySpeed = 120.0;

var yMinLimit = -20;
var yMaxLimit = 80;

private var x = 0.0;
private var y = 0.0;

var height = 5.0;

var heightDamping = 2.0;
var rotationDamping = 3.0;

private var scaleSmaller : System.Boolean;
private var scaleActual : System.Boolean;
private var scaleBigger : System.Boolean;

private var follow : System.Boolean;
private var rotate : System.Boolean;
private var rotateAround : System.Boolean;
private var range : float;
private var dir : Vector3;

private var drawOrbit : System.Boolean = true;
private var drawOrbitHeight : System.Boolean = false;

private var scrollPosition : Vector2 = Vector2.zero;

public var serverCtrl : ServerController;

function OnGUI ()
{
    GUI.skin = skin;
    var screenScale = Screen.height/768.0;
    
    if(GUI.Button (Rect (300*screenScale + 40,10,100*screenScale,44*screenScale), "Draw orbit"))
    {
        drawOrbit = !drawOrbit;
        var gos : GameObject[];
        gos = GameObject.FindGameObjectsWithTag("orbit");
        for(var o : GameObject in gos)
        {
            var line : LineRenderer = o.GetComponent(LineRenderer);
            line.enabled = drawOrbit;
        }
        if(drawOrbit == false)
        {
            drawOrbitHeight = false;
            var orbitHeights : GameObject[];
            orbitHeights = GameObject.FindGameObjectsWithTag("orbitHeight");
            for(var oH : GameObject in orbitHeights)
            {
                var lineH : LineRenderer = oH.GetComponent(LineRenderer);
                lineH.enabled = false;
            }
        }
    }
    if(GUI.Button (Rect (400*screenScale + 40,10,100*screenScale,44*screenScale), "Draw orbit height"))
    {
        drawOrbitHeight = !drawOrbitHeight;
        var orbitHeights2 : GameObject[];
        orbitHeights2 = GameObject.FindGameObjectsWithTag("orbitHeight");
        for(var oH2 : GameObject in orbitHeights2)
        {
            var lineH2 : LineRenderer = oH2.GetComponent(LineRenderer);
            lineH2.enabled = drawOrbitHeight;
        }
        if(drawOrbitHeight == true)
        {
            drawOrbit = true;
            var gos2 : GameObject[];
            gos2 = GameObject.FindGameObjectsWithTag("orbit");
            for(var o2 : GameObject in gos2)
            {
                var line2 : LineRenderer = o2.GetComponent(LineRenderer);
                line2.enabled = drawOrbit;
            }
        }
    }
    
    // Make a main menu
    if(GUI.Button (Rect (20,10,300*screenScale,44*screenScale), "Solar System"))
    {
        sl.sliderValue = 0.0;
        //set actual size of planets
        sun.transform.localScale = new Vector3(5,5,5);
        mercury.transform.localScale = new Vector3(3,3,3);
        venus.transform.localScale = new Vector3(2,2,2);
        earth.transform.localScale = new Vector3(2,2,2);
    	mars.transform.localScale = new Vector3(1,1,1);
    	jupiter.transform.localScale = new Vector3(4,4,4);
    	saturn.transform.localScale = new Vector3(3.5,3.5,3.5);
    	uranus.transform.localScale = new Vector3(3,3,3);
        neptune.transform.localScale = new Vector3(3,3,3);
        pluto.transform.localScale = new Vector3(1,1,1);
        
        moon.transform.localScale = new Vector3(0.3,0.3,0.3);
        calisto.transform.localScale = new Vector3(0.2,0.2,0.2);
        europa.transform.localScale = new Vector3(0.15,0.15,0.15);
        ganymede.transform.localScale = new Vector3(0.25,0.25,0.25);
        lo.transform.localScale = new Vector3(0.25,0.25,0.25);
        demios.transform.localScale = new Vector3(0.1,0.1,0.1);
        phobos.transform.localScale = new Vector3(0.1,0.1,0.1);
        diona.transform.localScale = new Vector3(0.06,0.06,0.06);
        enceladus.transform.localScale = new Vector3(0.05,0.05,0.05);
        mimas.transform.localScale = new Vector3(0.045,0.045,0.045);
        rhea.transform.localScale = new Vector3(0.09,0.09,0.09);
        tehys.transform.localScale = new Vector3(0.058,0.058,0.058);
        titan.transform.localScale = new Vector3(0.28,0.28,0.28);
        //----
        menu = !menu;
        if(target && target.audio.isPlaying)
            target.audio.Stop();
        if(target && !follow)
		{
			moveBack = true;
			if(target.audio.isPlaying)
        	{
        		target.audio.Stop();
        	}
         	info = false;
 			satelites = false;
        	images = false;
		}
        scaleSmaller = false;
        scaleActual =  true;
        scaleBigger = false;
    }
    GUI.skin.button.hover.textColor = Color.gray;
    
    if(menu)
    {
        //GUI.Box(Rect(20,10,300*screenScale,484*screenScale),"");
        
        if(GUI.Button (Rect (20,44*screenScale+10,150*screenScale,44*screenScale), "Sun")&& !sunKey)
        {
            sunKey = !sunKey;
            marsKey = false;
            earthKey = false;
            mercuryKey = false;
            jupiterKey = false;
            neptuneKey = false;
            plutoKey = false;
            saturnKey = false;
            uranusKey = false;
            venusKey = false;
            //satelites -----
            moonKey = false;
            calistoKey = false;
            europaKey = false;
            ganymedeKey = false;
            loKey = false;
            demiosKey = false;
            phobosKey = false;
            dionaKey = false;
            enceladusKey = false;
            mimasKey = false;
            rheaKey = false;
            tehysKey = false;
            titanKey = false;
            //----------------
            menu = false;
            distance = 8 + sun.transform.localScale.x;
            tmpDistance = distance;
            target = sun.transform;
            follow = true;
            rotateAround = false;
            effect = false;
            index = 0;
            indexForMenu = 0;
            //send id to server
            var cs : Planet = target.gameObject.GetComponent(Planet);
            Debug.Log(cs.ID);
            serverCtrl.SelectPlanet(cs.ID);
            //-------
        }
        GUI.skin.button.hover.textColor = Color.gray;
        
        if(GUI.Button (Rect (20,88*screenScale+10,150*screenScale,44*screenScale), "Mercury")&& !mercuryKey)
        {
            mercuryKey = !mercuryKey;
            marsKey = false;
            earthKey = false;
            sunKey = false;
            jupiterKey = false;
            neptuneKey = false;
            plutoKey = false;
            saturnKey = false;
            uranusKey = false;
            venusKey = false;
            //satelites -----
            moonKey = false;
            calistoKey = false;
            europaKey = false;
            ganymedeKey = false;
            loKey = false;
            demiosKey = false;
            phobosKey = false;
            dionaKey = false;
            enceladusKey = false;
            mimasKey = false;
            rheaKey = false;
            tehysKey = false;
            titanKey = false;
            //----------------
            menu = false;
            distance = 8 + mercury.transform.localScale.x;
            tmpDistance = distance;
            target = mercury.transform;
            follow = true;
            rotateAround = false;
            effect = false;
            index = 0;
            indexForMenu = 0;
            //send id to server
            cs = target.gameObject.GetComponent(Planet);
            Debug.Log(cs.ID);
            serverCtrl.SelectPlanet(cs.ID);
            //-------
        }
        GUI.skin.button.hover.textColor = Color.gray;
        
        if(GUI.Button (Rect (20,132*screenScale+10,150*screenScale,44*screenScale), "Venus")&& !venusKey)
        {
            venusKey = !venusKey;
            marsKey = false;
            mercuryKey = false;
            earthKey = false;
            sunKey = false;
            jupiterKey = false;
            neptuneKey = false;
            plutoKey = false;
            saturnKey = false;
            uranusKey = false;
            //satelites -----
            moonKey = false;
            calistoKey = false;
            europaKey = false;
            ganymedeKey = false;
            loKey = false;
            demiosKey = false;
            phobosKey = false;
            dionaKey = false;
            enceladusKey = false;
            mimasKey = false;
            rheaKey = false;
            tehysKey = false;
            titanKey = false;
            //----------------
            menu = false;
            distance = 8 + venus.transform.localScale.x;
            tmpDistance = distance;
            target = venus.transform;
            follow = true;
            rotateAround = false;
            effect = false;
            index = 0;
            indexForMenu = 0;
            //send id to server
            cs = target.gameObject.GetComponent(Planet);
            Debug.Log(cs.ID);
            serverCtrl.SelectPlanet(cs.ID);
            //-------
        }
        GUI.skin.button.hover.textColor = Color.gray;
        
        if(GUI.Button (Rect (20,176*screenScale+10,150*screenScale,44*screenScale), "Earth")&& !earthKey)
        {
            earthKey = !earthKey;
            marsKey = false;
            mercuryKey = false;
            sunKey = false;
            mercuryKey = false;
            jupiterKey = false;
            neptuneKey = false;
            plutoKey = false;
            saturnKey = false;
            uranusKey = false;
            venusKey = false;
            //satelites -----
            moonKey = false;
            calistoKey = false;
            europaKey = false;
            ganymedeKey = false;
            loKey = false;
            demiosKey = false;
            phobosKey = false;
            dionaKey = false;
            enceladusKey = false;
            mimasKey = false;
            rheaKey = false;
            tehysKey = false;
            titanKey = false;
            //----------------
            menu = false;
            distance = 6 + earth.transform.localScale.x;
            tmpDistance = distance;
            target = earth.transform;
            follow = true;
            rotateAround = false;
            effect = false;
            index = 0;
            indexForMenu = 0;
            //send id to server
            cs = target.gameObject.GetComponent(Planet);
            Debug.Log(cs.ID);
            serverCtrl.SelectPlanet(cs.ID);
            //-------
        }
        GUI.skin.button.hover.textColor = Color.gray;
        
        if(GUI.Button (Rect (20,220*screenScale+10,150*screenScale,44*screenScale), "Mars") && !marsKey)
        {
            marsKey = !marsKey;
            earthKey = false;
            sunKey = false;
            mercuryKey = false;
            jupiterKey = false;
            neptuneKey = false;
            plutoKey = false;
            saturnKey = false;
            uranusKey = false;
            venusKey = false;
            //satelites -----
            moonKey = false;
            calistoKey = false;
            europaKey = false;
            ganymedeKey = false;
            loKey = false;
            demiosKey = false;
            phobosKey = false;
            dionaKey = false;
            enceladusKey = false;
            mimasKey = false;
            rheaKey = false;
            tehysKey = false;
            titanKey = false;
            //----------------
            menu = false;
            distance = 8 + mars.transform.localScale.x;
            tmpDistance = distance;
            target = mars.transform;
            follow = true;
            rotateAround = false;
            effect = false;
            index = 0;
            indexForMenu = 0;
            //send id to server
            cs = target.gameObject.GetComponent(Planet);
            Debug.Log(cs.ID);
            serverCtrl.SelectPlanet(cs.ID);
            //-------
        }
        GUI.skin.button.hover.textColor = Color.gray;
        
        if(GUI.Button (Rect (20,264*screenScale+10,150*screenScale,44*screenScale), "Jupiter")&& !jupiterKey)
        {
            jupiterKey = !jupiterKey;
            marsKey = false;
            mercuryKey = false;
            earthKey = false;
            sunKey = false;
            neptuneKey = false;
            plutoKey = false;
            saturnKey = false;
            uranusKey = false;
            venusKey = false;
            //satelites -----
            moonKey = false;
            calistoKey = false;
            europaKey = false;
            ganymedeKey = false;
            loKey = false;
            demiosKey = false;
            phobosKey = false;
            dionaKey = false;
            enceladusKey = false;
            mimasKey = false;
            rheaKey = false;
            tehysKey = false;
            titanKey = false;
            //----------------
            menu = false;
            distance = 8 + jupiter.transform.localScale.x;
            tmpDistance = distance;
            target = jupiter.transform;
            follow = true;
            rotateAround = false;
            effect = false;
            index = 0;
            indexForMenu = 0;
            //send id to server
            cs = target.gameObject.GetComponent(Planet);
            Debug.Log(cs.ID);
            serverCtrl.SelectPlanet(cs.ID);
            //-------
        }
        GUI.skin.button.hover.textColor = Color.gray;
        
        if(GUI.Button (Rect (20,308*screenScale+10,150*screenScale,44*screenScale), "Saturn")&& !saturnKey)
        {
            saturnKey = !saturnKey;
            marsKey = false;
            mercuryKey = false;
            earthKey = false;
            sunKey = false;
            jupiterKey = false;
            neptuneKey = false;
            plutoKey = false;
            uranusKey = false;
            venusKey = false;
            //satelites -----
            moonKey = false;
            calistoKey = false;
            europaKey = false;
            ganymedeKey = false;
            loKey = false;
            demiosKey = false;
            phobosKey = false;
            dionaKey = false;
            enceladusKey = false;
            mimasKey = false;
            rheaKey = false;
            tehysKey = false;
            titanKey = false;
            //----------------
            menu = false;
            distance = 8 + saturn.transform.localScale.x;
            tmpDistance = distance;
            target = saturn.transform;
            follow = true;
            rotateAround = false;
            effect = false;
            index = 0;
            indexForMenu = 0;
            //send id to server
            cs = target.gameObject.GetComponent(Planet);
            Debug.Log(cs.ID);
            serverCtrl.SelectPlanet(cs.ID);
            //-------
        }
        GUI.skin.button.hover.textColor = Color.gray;
        
        if(GUI.Button (Rect (20,352*screenScale+10,150*screenScale,44*screenScale), "Uranus")&& !uranusKey)
        {
            uranusKey = !uranusKey;
            marsKey = false;
            mercuryKey = false;
            earthKey = false;
            sunKey = false;
            jupiterKey = false;
            neptuneKey = false;
            plutoKey = false;
            saturnKey = false;
            venusKey = false;
            //satelites -----
            moonKey = false;
            calistoKey = false;
            europaKey = false;
            ganymedeKey = false;
            loKey = false;
            demiosKey = false;
            phobosKey = false;
            dionaKey = false;
            enceladusKey = false;
            mimasKey = false;
            rheaKey = false;
            tehysKey = false;
            titanKey = false;
            //----------------
            menu = false;
            distance = 8 + uranus.transform.localScale.x;
            tmpDistance = distance;
            target = uranus.transform;
            follow = true;
            rotateAround = false;
            effect = false;
            index = 0;
            indexForMenu = 0;
            //send id to server
            cs = target.gameObject.GetComponent(Planet);
            Debug.Log(cs.ID);
            serverCtrl.SelectPlanet(cs.ID);
            //-------
        }
        GUI.skin.button.hover.textColor = Color.gray;
        
        if(GUI.Button (Rect (20,396*screenScale+10,150*screenScale,44*screenScale), "Neptune")&& !neptuneKey)
        {
            neptuneKey = !neptuneKey;
            marsKey = false;
            mercuryKey = false;
            earthKey = false;
            sunKey = false;
            jupiterKey = false;
            plutoKey = false;
            saturnKey = false;
            uranusKey = false;
            venusKey = false;
            //satelites -----
            moonKey = false;
            calistoKey = false;
            europaKey = false;
            ganymedeKey = false;
            loKey = false;
            demiosKey = false;
            phobosKey = false;
            dionaKey = false;
            enceladusKey = false;
            mimasKey = false;
            rheaKey = false;
            tehysKey = false;
            titanKey = false;
            //----------------
            menu = false;
            distance = 8 + neptune.transform.localScale.x;
            tmpDistance = distance;
            target = neptune.transform;
            follow = true;
            rotateAround = false;
            effect = false;
            index = 0;
            indexForMenu = 0;
            //send id to server
            cs = target.gameObject.GetComponent(Planet);
            Debug.Log(cs.ID);
            serverCtrl.SelectPlanet(cs.ID);
            //-------
        }
        GUI.skin.button.hover.textColor = Color.gray;
        
        if(GUI.Button (Rect (20,440*screenScale+10,150*screenScale,44*screenScale), "Pluto")&& !plutoKey)
        {
            plutoKey = !plutoKey;
            marsKey = false;
            mercuryKey = false;
            earthKey = false;
            sunKey = false;
            jupiterKey = false;
            neptuneKey = false;
            saturnKey = false;
            uranusKey = false;
            venusKey = false;
            //satelites -----
            moonKey = false;
            calistoKey = false;
            europaKey = false;
            ganymedeKey = false;
            loKey = false;
            demiosKey = false;
            phobosKey = false;
            dionaKey = false;
            enceladusKey = false;
            mimasKey = false;
            rheaKey = false;
            tehysKey = false;
            titanKey = false;
            //----------------
            menu = false;
            distance = 4 + pluto.transform.localScale.x;
            tmpDistance = distance;
            target = pluto.transform;
            follow = true;
            rotateAround = false;
            effect = false;
            index = 0;
            indexForMenu = 0;
            //send id to server
            cs = target.gameObject.GetComponent(Planet);
            Debug.Log(cs.ID);
            serverCtrl.SelectPlanet(cs.ID);
            //-------
        }
        GUI.skin.button.hover.textColor = Color.gray;
        
        //menu for satelites
        if(GUI.Button (Rect (20+150*screenScale,44*screenScale+10,150*screenScale,44*screenScale), "Moon")&& !moonKey)
        {
            sunKey = false;
            marsKey = false;
            earthKey = false;
            mercuryKey = false;
            jupiterKey = false;
            neptuneKey = false;
            plutoKey = false;
            saturnKey = false;
            uranusKey = false;
            venusKey = false;
            //satelites -----
            moonKey = !moonKey;
            calistoKey = false;
            europaKey = false;
            ganymedeKey = false;
            loKey = false;
            demiosKey = false;
            phobosKey = false;
            dionaKey = false;
            enceladusKey = false;
            mimasKey = false;
            rheaKey = false;
            tehysKey = false;
            titanKey = false;
            //----------------
            menu = false;
            distance = 4 + moon.transform.localScale.x;
            tmpDistance = distance;
            target = moon.transform;
            follow = true;
            rotateAround = false;
            effect = false;
            index = 0;
            indexForMenu = 0;
            //send id to server
            cs = target.gameObject.GetComponent(Planet);
            Debug.Log(cs.ID);
            serverCtrl.SelectPlanet(cs.ID);
            //-------
        }
        GUI.skin.button.hover.textColor = Color.gray;
        
        if(GUI.Button (Rect (20+150*screenScale,88*screenScale+10,150*screenScale,44*screenScale), "Calisto")&& !calistoKey)
        {
            sunKey = false;
            marsKey = false;
            earthKey = false;
            mercuryKey = false;
            jupiterKey = false;
            neptuneKey = false;
            plutoKey = false;
            saturnKey = false;
            uranusKey = false;
            venusKey = false;
            //satelites -----
            moonKey = false;
            calistoKey = !calistoKey;
            europaKey = false;
            ganymedeKey = false;
            loKey = false;
            demiosKey = false;
            phobosKey = false;
            dionaKey = false;
            enceladusKey = false;
            mimasKey = false;
            rheaKey = false;
            tehysKey = false;
            titanKey = false;
            //----------------
            menu = false;
            distance = 4 + calisto.transform.localScale.x;
            tmpDistance = distance;
            target = calisto.transform;
            follow = true;
            rotateAround = false;
            effect = false;
            index = 0;
            indexForMenu = 0;
            //send id to server
            cs = target.gameObject.GetComponent(Planet);
            Debug.Log(cs.ID);
            serverCtrl.SelectPlanet(cs.ID);
            //-------
        }
        GUI.skin.button.hover.textColor = Color.gray;
        
        if(GUI.Button (Rect (20+150*screenScale,132*screenScale+10,150*screenScale,44*screenScale), "Europa")&& !europaKey)
        {
            sunKey = false;
            marsKey = false;
            earthKey = false;
            mercuryKey = false;
            jupiterKey = false;
            neptuneKey = false;
            plutoKey = false;
            saturnKey = false;
            uranusKey = false;
            venusKey = false;
            //satelites -----
            moonKey = false;
            calistoKey = false;
            europaKey = !europaKey;
            ganymedeKey = false;
            loKey = false;
            demiosKey = false;
            phobosKey = false;
            dionaKey = false;
            enceladusKey = false;
            mimasKey = false;
            rheaKey = false;
            tehysKey = false;
            titanKey = false;
            //----------------
            menu = false;
            distance = 4 + europa.transform.localScale.x;
            tmpDistance = distance;
            target = europa.transform;
            follow = true;
            rotateAround = false;
            effect = false;
            index = 0;
            indexForMenu = 0;
            //send id to server
            cs = target.gameObject.GetComponent(Planet);
            Debug.Log(cs.ID);
            serverCtrl.SelectPlanet(cs.ID);
            //-------
        }
        GUI.skin.button.hover.textColor = Color.gray;
        
        if(GUI.Button (Rect (20+150*screenScale,176*screenScale+10,150*screenScale,44*screenScale), "Ganymede")&& !ganymedeKey)
        {
            sunKey = false;
            marsKey = false;
            earthKey = false;
            mercuryKey = false;
            jupiterKey = false;
            neptuneKey = false;
            plutoKey = false;
            saturnKey = false;
            uranusKey = false;
            venusKey = false;
            //satelites -----
            moonKey = false;
            calistoKey = false;
            europaKey = false;
            ganymedeKey = !ganymedeKey;
            loKey = false;
            demiosKey = false;
            phobosKey = false;
            dionaKey = false;
            enceladusKey = false;
            mimasKey = false;
            rheaKey = false;
            tehysKey = false;
            titanKey = false;
            //----------------
            menu = false;
            distance = 5 + ganymede.transform.localScale.x;
            tmpDistance = distance;
            target = ganymede.transform;
            follow = true;
            rotateAround = false;
            effect = false;
            index = 0;
            indexForMenu = 0;
            //send id to server
            cs = target.gameObject.GetComponent(Planet);
            Debug.Log(cs.ID);
            serverCtrl.SelectPlanet(cs.ID);
            //-------
        }
        GUI.skin.button.hover.textColor = Color.gray;
        
        if(GUI.Button (Rect (20+150*screenScale,220*screenScale+10,150*screenScale,44*screenScale), "Lo")&& !loKey)
        {
            sunKey = false;
            marsKey = false;
            earthKey = false;
            mercuryKey = false;
            jupiterKey = false;
            neptuneKey = false;
            plutoKey = false;
            saturnKey = false;
            uranusKey = false;
            venusKey = false;
            //satelites -----
            moonKey = false;
            calistoKey = false;
            europaKey = false;
            ganymedeKey = false;
            loKey = !loKey;
            demiosKey = false;
            phobosKey = false;
            dionaKey = false;
            enceladusKey = false;
            mimasKey = false;
            rheaKey = false;
            tehysKey = false;
            titanKey = false;
            //----------------
            menu = false;
            distance = 5 + lo.transform.localScale.x;
            tmpDistance = distance;
            target = lo.transform;
            follow = true;
            rotateAround = false;
            effect = false;
            index = 0;
            indexForMenu = 0;
            //send id to server
            cs = target.gameObject.GetComponent(Planet);
            Debug.Log(cs.ID);
            serverCtrl.SelectPlanet(cs.ID);
            //-------
        }
        GUI.skin.button.hover.textColor = Color.gray;
        
        if(GUI.Button (Rect (20+150*screenScale,264*screenScale+10,150*screenScale,44*screenScale), "Demios")&& !demiosKey)
        {
            sunKey = false;
            marsKey = false;
            earthKey = false;
            mercuryKey = false;
            jupiterKey = false;
            neptuneKey = false;
            plutoKey = false;
            saturnKey = false;
            uranusKey = false;
            venusKey = false;
            //satelites -----
            moonKey = false;
            calistoKey = false;
            europaKey = false;
            ganymedeKey = false;
            loKey = false;
            demiosKey = !demiosKey;
            phobosKey = false;
            dionaKey = false;
            enceladusKey = false;
            mimasKey = false;
            rheaKey = false;
            tehysKey = false;
            titanKey = false;
            //----------------
            menu = false;
            distance = 2 + demios.transform.localScale.x;
            tmpDistance = distance;
            target = demios.transform;
            follow = true;
            rotateAround = false;
            effect = false;
            index = 0;
            indexForMenu = 0;
            //send id to server
            cs = target.gameObject.GetComponent(Planet);
            Debug.Log(cs.ID);
            serverCtrl.SelectPlanet(cs.ID);
            //-------
        }
        GUI.skin.button.hover.textColor = Color.gray;
        
        if(GUI.Button (Rect (20+150*screenScale,308*screenScale+10,150*screenScale,44*screenScale), "Phobos")&& !phobosKey)
        {
            sunKey = false;
            marsKey = false;
            earthKey = false;
            mercuryKey = false;
            jupiterKey = false;
            neptuneKey = false;
            plutoKey = false;
            saturnKey = false;
            uranusKey = false;
            venusKey = false;
            //satelites -----
            moonKey = false;
            calistoKey = false;
            europaKey = false;
            ganymedeKey = false;
            loKey = false;
            demiosKey = false;
            phobosKey = !phobosKey;
            dionaKey = false;
            enceladusKey = false;
            mimasKey = false;
            rheaKey = false;
            tehysKey = false;
            titanKey = false;
            //----------------
            menu = false;
            distance = 2 + phobos.transform.localScale.x;
            tmpDistance = distance;
            target = phobos.transform;
            follow = true;
            rotateAround = false;
            effect = false;
            index = 0;
            indexForMenu = 0;
            //send id to server
            cs = target.gameObject.GetComponent(Planet);
            Debug.Log(cs.ID);
            serverCtrl.SelectPlanet(cs.ID);
            //-------
        }
        GUI.skin.button.hover.textColor = Color.gray;
        
        if(GUI.Button (Rect (20+150*screenScale,352*screenScale+10,150*screenScale,44*screenScale), "Diona")&& !dionaKey)
        {
            sunKey = false;
            marsKey = false;
            earthKey = false;
            mercuryKey = false;
            jupiterKey = false;
            neptuneKey = false;
            plutoKey = false;
            saturnKey = false;
            uranusKey = false;
            venusKey = false;
            //satelites -----
            moonKey = false;
            calistoKey = false;
            europaKey = false;
            ganymedeKey = false;
            loKey = false;
            demiosKey = false;
            phobosKey = false;
            dionaKey = !dionaKey;
            enceladusKey = false;
            mimasKey = false;
            rheaKey = false;
            tehysKey = false;
            titanKey = false;
            //----------------
            menu = false;
            distance = 1 + diona.transform.localScale.x;
            tmpDistance = distance;
            target = diona.transform;
            follow = true;
            rotateAround = false;
            effect = false;
            index = 0;
            indexForMenu = 0;
            //send id to server
            cs = target.gameObject.GetComponent(Planet);
            Debug.Log(cs.ID);
            serverCtrl.SelectPlanet(cs.ID);
            //-------
        }
        GUI.skin.button.hover.textColor = Color.gray;
        
        if(GUI.Button (Rect (20+150*screenScale,396*screenScale+10,150*screenScale,44*screenScale), "Enceladus")&& !enceladusKey)
        {
            sunKey = false;
            marsKey = false;
            earthKey = false;
            mercuryKey = false;
            jupiterKey = false;
            neptuneKey = false;
            plutoKey = false;
            saturnKey = false;
            uranusKey = false;
            venusKey = false;
            //satelites -----
            moonKey = false;
            calistoKey = false;
            europaKey = false;
            ganymedeKey = false;
            loKey = false;
            demiosKey = false;
            phobosKey = false;
            dionaKey = false;
            enceladusKey = !enceladusKey;
            mimasKey = false;
            rheaKey = false;
            tehysKey = false;
            titanKey = false;
            //----------------
            menu = false;
            distance = 1 + enceladus.transform.localScale.x;
            tmpDistance = distance;
            target = enceladus.transform;
            follow = true;
            rotateAround = false;
            effect = false;
            index = 0;
            indexForMenu = 0;
            //send id to server
            cs = target.gameObject.GetComponent(Planet);
            Debug.Log(cs.ID);
            serverCtrl.SelectPlanet(cs.ID);
            //-------
        }
        GUI.skin.button.hover.textColor = Color.gray;
        
        if(GUI.Button (Rect (20+150*screenScale,440*screenScale+10,150*screenScale,44*screenScale), "Mimas")&& !mimasKey)
        {
            sunKey = false;
            marsKey = false;
            earthKey = false;
            mercuryKey = false;
            jupiterKey = false;
            neptuneKey = false;
            plutoKey = false;
            saturnKey = false;
            uranusKey = false;
            venusKey = false;
            //satelites -----
            moonKey = false;
            calistoKey = false;
            europaKey = false;
            ganymedeKey = false;
            loKey = false;
            demiosKey = false;
            phobosKey = false;
            dionaKey = false;
            enceladusKey = false;
            mimasKey = !mimasKey;
            rheaKey = false;
            tehysKey = false;
            titanKey = false;
            //----------------
            menu = false;
            distance = 1 + mimas.transform.localScale.x;
            tmpDistance = distance;
            target = mimas.transform;
            follow = true;
            rotateAround = false;
            effect = false;
            index = 0;
            indexForMenu = 0;
            //send id to server
            cs = target.gameObject.GetComponent(Planet);
            Debug.Log(cs.ID);
            serverCtrl.SelectPlanet(cs.ID);
            //-------
        }
        GUI.skin.button.hover.textColor = Color.gray;
        
        if(GUI.Button (Rect (20+150*screenScale,484*screenScale+10,150*screenScale,44*screenScale), "Rhea")&& !rheaKey)
        {
            sunKey = false;
            marsKey = false;
            earthKey = false;
            mercuryKey = false;
            jupiterKey = false;
            neptuneKey = false;
            plutoKey = false;
            saturnKey = false;
            uranusKey = false;
            venusKey = false;
            //satelites -----
            moonKey = false;
            calistoKey = false;
            europaKey = false;
            ganymedeKey = false;
            loKey = false;
            demiosKey = false;
            phobosKey = false;
            dionaKey = false;
            enceladusKey = false;
            mimasKey = false;
            rheaKey = !rheaKey;
            tehysKey = false;
            titanKey = false;
            //----------------
            menu = false;
            distance = 1 + rhea.transform.localScale.x;
            tmpDistance = distance;
            target = rhea.transform;
            follow = true;
            rotateAround = false;
            effect = false;
            index = 0;
            indexForMenu = 0;
            //send id to server
            cs = target.gameObject.GetComponent(Planet);
            Debug.Log(cs.ID);
            serverCtrl.SelectPlanet(cs.ID);
            //-------
        }
        GUI.skin.button.hover.textColor = Color.gray;
        
        if(GUI.Button (Rect (20+150*screenScale,528*screenScale+10,150*screenScale,44*screenScale), "Tehys")&& !tehysKey)
        {
            sunKey = false;
            marsKey = false;
            earthKey = false;
            mercuryKey = false;
            jupiterKey = false;
            neptuneKey = false;
            plutoKey = false;
            saturnKey = false;
            uranusKey = false;
            venusKey = false;
            //satelites -----
            moonKey = false;
            calistoKey = false;
            europaKey = false;
            ganymedeKey = false;
            loKey = false;
            demiosKey = false;
            phobosKey = false;
            dionaKey = false;
            enceladusKey = false;
            mimasKey = false;
            rheaKey = false;
            tehysKey = !tehysKey;
            titanKey = false;
            //----------------
            menu = false;
            distance = 1 + tehys.transform.localScale.x;
            tmpDistance = distance;
            target = tehys.transform;
            follow = true;
            rotateAround = false;
            effect = false;
            index = 0;
            indexForMenu = 0;
            //send id to server
            cs = target.gameObject.GetComponent(Planet);
            Debug.Log(cs.ID);
            serverCtrl.SelectPlanet(cs.ID);
            //-------
        }
        GUI.skin.button.hover.textColor = Color.gray;
        
        if(GUI.Button (Rect (20+150*screenScale,572*screenScale+10,150*screenScale,44*screenScale), "Titan")&& !titanKey)
        {
            sunKey = false;
            marsKey = false;
            earthKey = false;
            mercuryKey = false;
            jupiterKey = false;
            neptuneKey = false;
            plutoKey = false;
            saturnKey = false;
            uranusKey = false;
            venusKey = false;
            //satelites -----
            moonKey = false;
            calistoKey = false;
            europaKey = false;
            ganymedeKey = false;
            loKey = false;
            demiosKey = false;
            phobosKey = false;
            dionaKey = false;
            enceladusKey = false;
            mimasKey = false;
            rheaKey = false;
            tehysKey = false;
            titanKey = !titanKey;
            //----------------
            menu = false;
            distance = 2 + titan.transform.localScale.x;
            tmpDistance = distance;
            target = titan.transform;
            follow = true;
            rotateAround = false;
            effect = false;
            index = 0;
            indexForMenu = 0;
            //send id to server
            cs = target.gameObject.GetComponent(Planet);
            Debug.Log(cs.ID);
            serverCtrl.SelectPlanet(cs.ID);
            //-------
        }
        GUI.skin.button.hover.textColor = Color.gray;
    }
    
    //move camera forward and backward
    GUI.skin = skinSecond;
    var box : Texture2D = Resources.Load("mainBG") as Texture2D;
    //GUI.Box(new Rect(Screen.width/2 - 100,Screen.height - 200,200,200),box);
    var minus : Texture2D = Resources.Load("minus") as Texture2D;
    if(GUI.Button(new Rect(Screen.width/2 - 92.5,Screen.height - 33.5/*270*/,25,25),minus)  && !moveBack && ! showingDetails && !follow)
    {
        distance = distance + 1;
    }
    var plus : Texture2D = Resources.Load("plus") as Texture2D;
    
    if(GUI.Button(new Rect(Screen.width/2 + 67.5,Screen.height - 33.5/*270*/,25,25),plus) && !moveBack && !showingDetails && !follow)
    {
        if(target && distance > (target.transform.localScale.x/2)+2)
        {
            distance =  distance - 1;
        }
        
    }
    
    //make menu for scaling planet
    GUI.skin = btn4;
    GUI.Button(new Rect(Screen.width - 240*screenScale,Screen.height-60*screenScale,50*screenScale,50*screenScale),"");
    GUI.skin = btn1;
    GUI.Button(new Rect(Screen.width - 180*screenScale,Screen.height-60*screenScale,50*screenScale,50*screenScale),"");
    GUI.skin = btn3;
    GUI.Button(new Rect(Screen.width - 120*screenScale,Screen.height-60*screenScale,50*screenScale,50*screenScale),"");
    GUI.skin = btn2;
    GUI.Button(new Rect(Screen.width - 60*screenScale,Screen.height-60*screenScale,50*screenScale,50*screenScale),"");
    GUI.skin = skin;
//    if(GUI.Button(new Rect(Screen.width - 255,Screen.height-60,75,50),"small") && !moveBack && !showingDetails && !follow)
//    {
//        scaleSmaller = true;
//        scaleActual = false;
//        scaleBigger = false;
//        drawGUIforPlanet = false;
//    }
//    if(GUI.Button(new Rect(Screen.width - 170,Screen.height-60,75,50),"medium") && !moveBack && !showingDetails && !follow)
//    {
//        scaleSmaller = false;
//        scaleActual = true;
//        scaleBigger = false;
//        if(!info)
//            drawGUIforPlanet = true;
//    }
//    if(GUI.Button(new Rect(Screen.width - 85,Screen.height-60,75,50),"big") && !moveBack && !showingDetails && !follow)
//    {
//        scaleSmaller = false;
//        scaleActual = false;
//        scaleBigger = true;
//        drawGUIforPlanet = false;
//    }
    
    //menu for planet
    if(target)
    {
        var s : MP = target.Find("movie").GetComponent(MP);
    }
    if(drawGUIforPlanet && !showingDetails)
    {
        //Debug.Log("Draw GUI for planet");
        var screenPos : Vector3 = camera.WorldToScreenPoint (target.position);
		var menuStep =  target.transform.localScale.x/2;
		if(menuStep < 1)
			menuStep = 1;
        if(effect)
        {
            //Debug.Log("Show effects");
            var imgName : String = null;
            if(index <= 36)
                imgName = index.ToString();
            else
            {
                var tmpIndex = index - 37;
                imgName = "loop _"+ tmpIndex.ToString();
            }
            var FXtexture : Texture2D = Resources.Load(imgName) as Texture2D;
            var size : float = (Screen.width/5)*(target.transform.localScale.x);
            var rect : Rect = getSize();
            GUI.DrawTexture(new Rect(screenPos.x - rect.width/2,screenPos.y - rect.height/2,rect.width,rect.height), FXtexture, ScaleMode.ScaleAndCrop);
            if(index >36)
            {
                var imgNameMenu : String = null;
                if(indexForMenu <= 38)
                    imgNameMenu = "option_circle_reveal_"+ indexForMenu.ToString();
                else
                {
                    imgNameMenu = "option_circle_loop_"+ indexForMenu.ToString();
                }
                var FXtextureMenu : Texture2D = Resources.Load(imgNameMenu) as Texture2D;
                GUI.DrawTexture(new Rect(screenPos.x-/*((Screen.width/10)*menuStep)*/rect.width/4-70-25,screenPos.y+rect.width/8 + 15-25,100,100), FXtextureMenu, ScaleMode.ScaleAndCrop);
                GUI.DrawTexture(new Rect(screenPos.x-/*((Screen.width/10)*menuStep)*/rect.width/4-50-25,screenPos.y+rect.width/8 + 75-25,100,100), FXtextureMenu, ScaleMode.ScaleAndCrop);
//                GUI.DrawTexture(new Rect(screenPos.x+/*((Screen.width/10)*menuStep)*/rect.width/4+60-25,Screen.height/2 - 25-25,100,100), FXtextureMenu, ScaleMode.ScaleAndCrop);
                GUI.DrawTexture(new Rect(screenPos.x+/*((Screen.width/10)*menuStep)*/rect.width/4+20-25,screenPos.y+rect.width/8 + 15-25,100,100), FXtextureMenu, ScaleMode.ScaleAndCrop);
                GUI.DrawTexture(new Rect(screenPos.x+/*((Screen.width/10)*menuStep)*/rect.width/4-25,screenPos.y+rect.width/8 + 75-25,100,100), FXtextureMenu, ScaleMode.ScaleAndCrop);
                
                if(indexForMenu > 38)
                {
                    //Debug.Log("menu");
                    if(GUI.Button(new Rect(screenPos.x-/*((Screen.width/10)*menuStep)*/rect.width/4-70 ,screenPos.y+rect.width/8 + 15,50,50),"info"))
                    {
                        sl.sliderValue = 0.0;
                        moveBack = true;
                        info = !info;
                        satelites = false;
                        if(target.audio.isPlaying)
                        {
                            target.audio.Stop();
                        }
                        s.playMovie = false;
                        images = false;
                    }
                    if(GUI.Button(new Rect(screenPos.x-/*((Screen.width/10)*menuStep)*/rect.width/4-50,screenPos.y+rect.width/8 + 75,50,50),"voice"))
                    {
                        sl.sliderValue = 0.0;
                        moveBack = true;
                        if(target.audio.isPlaying)
                        {
                            target.audio.Stop();
                        }
                        else
                        {
                            target.audio.Play();
                        }
                        info = false;
                        satelites = false;
                        s.playMovie = false;
                        images = false;
                    }
                    if(GUI.Button(new Rect(screenPos.x+/*((Screen.width/10)*menuStep)*/rect.width/4+20,screenPos.y+rect.width/8 + 15,50,50),"video"))
                       {
                       sl.sliderValue = 0.0;
                       moveBack = true;
                       if(target.audio.isPlaying)
                       {
                       target.audio.Stop();
                       }
                       s.playMovie = true;
                       info = false;
                       satelites = false;
                       images = false;
                       if(sunKey)
                       {
                       //set the video name
                       s.videoName = "videoTest.mp4";
                       }
                       if(earthKey)
                       {
                       s.videoName = "videoTest.mp4";
                       }
                       if(marsKey)
                       {
                       s.videoName = "videoTest.mp4";
                       }
                       if(mercuryKey)
                       {
                       s.videoName = "videoTest.mp4";
                       }
                       if(plutoKey)
                       {
                       s.videoName = "videoTest.mp4";
                       }
                       if(jupiterKey)
                       {
                       s.videoName = "videoTest.mp4";
                       }
                       if(neptuneKey)
                       {
                       s.videoName = "videoTest.mp4";
                       }
                       if(saturnKey)
                       {
                       s.videoName = "videoTest.mp4";
                       }
                       if(uranusKey)
                       {
                       s.videoName = "videoTest.mp4";
                       }
                       if(venusKey)
                       {
                       s.videoName = "videoTest.mp4";
                       }
                       if(moonKey)
                       {
                       s.videoName = "videoTest.mp4";
                       }
                       if(calistoKey)
                       {
                       s.videoName = "videoTest.mp4";
                       }
                       if(europaKey)
                       {
                       s.videoName = "videoTest.mp4";
                       }
                       if(ganymedeKey)
                       {
                       s.videoName = "videoTest.mp4";
                       }
                       if(loKey)
                       {
                       s.videoName = "videoTest.mp4";
                       }
                       if(demiosKey)
                       {
                       s.videoName = "videoTest.mp4";
                       }
                       if(phobosKey)
                       {
                       s.videoName = "videoTest.mp4";
                       }
                       if(dionaKey)
                       {
                       s.videoName = "videoTest.mp4";
                       }
                       if(enceladusKey)
                       {
                       s.videoName = "videoTest.mp4";
                       }
                       if(mimasKey)
                       {
                       s.videoName = "videoTest.mp4";
                       }
                       if(rheaKey)
                       {
                       s.videoName = "videoTest.mp4";
                       }
                       if(tehysKey)
                       {
                       s.videoName = "videoTest.mp4";
                       }
                       if(titanKey)
                       {
                       s.videoName = "videoTest.mp4";
                       }
                       }
                       //                    if(GUI.Button(new Rect(screenPos.x+/*((Screen.width/10)*menuStep)*/rect.width/4+40,Screen.height/2 + 35,50,50),"satelites"))
                       //                    {
                       //                        sl.sliderValue = 0.0;
                       //                        moveBack = true;
                       //                        satelites = !satelites;
                       //                        if(target.audio.isPlaying)
                       //                        {
                       //                            target.audio.Stop();
                       //                        }
                       //                        info = false;
                       //                        s.playMovie = false;
                       //                        images = false;
                       //                    }
                       if(GUI.Button(new Rect(screenPos.x+/*((Screen.width/10)*menuStep)*/rect.width/4,screenPos.y+rect.width/8 + 75,50,50),"menu"))
                       {
                       //                        sl.sliderValue = 0.0;
                       //                        moveBack = true;
                       //                        images = !images;
                       //                        if(target.audio.isPlaying)
                       //                        {
                       //                            target.audio.Stop();
                       //                        }
                       //                        info = false;
                       //                        satelites = false;
                       //                        s.playMovie = false;
                       }
                }
            }
        }

    }
    if(info && !moveBack)
    {
        showingDetails = true;
        var rightStyle = GUI.skin.GetStyle("Label");
        var leftStyle = GUI.skin.GetStyle("Label");
        var planetName : String;
        var description : String;
        if(sunKey)
        {
            planetName = "Солнце";
            description = "Солнце — звезда Солнечной системы и её главный компонент. Его масса (332 900 масс Земли)[38] достаточно велика для поддержания термоядерной реакции в его недрах[39], при которой высвобождается большое количество энергии, излучаемой в пространство в основном в виде электромагнитного излучения, максимум которого приходится на диапазон длин волн 400—700 нм, соответствующий видимому свету[40]. По звёздной классификации Солнце — типичный жёлтый карлик класса G2. Это название может ввести в заблуждение, так как по сравнению с большинством звёзд в нашей Галактике Солнце — довольно большая и яркая звезда[41]. Класс звезды определяется её положением на диаграмме Герцшпрунга — Рассела, которая показывает зависимость между яркостью звёзд и температурой их поверхности. Обычно более горячие звёзды являются более яркими. Бо́льшая часть звёзд находится на так называемой главной последовательности этой диаграммы, Солнце расположено примерно в середине этой последовательности. Более яркие и горячие, чем Солнце, звёзды сравнительно редки, а более тусклые и холодные звёзды (красные карлики) встречаются часто, составляя 85 % звёзд в Галактике[41][42]. Положение Солнца на главной последовательности показывает, что оно ещё не исчерпало свой запас водорода для ядерного синтеза и находится примерно в середине своей эволюции. Сейчас Солнце постепенно становится более ярким, на более ранних стадиях развития его яркость составляла лишь 70 % от сегодняшней[43]. Солнце — звезда I типа звёздного населения, оно образовалось на сравнительно поздней ступени развития Вселенной и поэтому характеризуется бо́льшим содержанием элементов тяжелее водорода и гелия (в астрономии принято называть такие элементы «металлами»), чем более старые звёзды II типа[44]. Элементы более тяжёлые, чем водород и гелий, формируются в ядрах первых звёзд, поэтому, прежде чем Вселенная могла быть обогащена этими элементами, должно было пройти первое поколение звёзд. Самые старые звёзды содержат мало металлов, а более молодые звёзды содержат их больше. Предполагается, что высокая металличность была крайне важна для образования у Солнца планетной системы, потому что планеты формируются аккрецией «металлов»[45].";
        }
        if(earthKey)
        {
            planetName = "Земля";
            description = "Земля является крупнейшей и самой плотной из внутренних планет. У Земли наблюдается тектоника плит. Вопрос о наличии жизни где-либо, кроме Земли, остаётся открытым[69]. Однако среди планет земной группы Земля является уникальной (прежде всего — гидросферой). Атмосфера Земли радикально отличается от атмосфер других планет — она содержит свободный кислород[70]. У Земли есть один естественный спутник — Луна, единственный большой спутник планет земной группы Солнечной системы.";
        }
        if(marsKey)
        {
            planetName = "Марс";
            description = "Марс меньше Земли и Венеры (0,107 массы Земли). Он обладает атмосферой, состоящей главным образом из углекислого газа, с поверхностным давлением 6,1 мбар (0,6 % от земного)[71]. На его поверхности есть вулканы, самый большой из которых, Олимп, превышает размерами все земные вулканы, достигая высоты 21,2 км[72]. Рифтовые впадины (долины Маринер) наряду с вулканами свидетельствуют о прошлой геологической активности, которая, по современным данным, окончилась около 2 млн лет назад[73]. Красный цвет поверхности Марса вызван большим количеством оксида железа в его грунте[74]. У планеты есть два спутника — Фобос и Деймос. Предполагается, что они являются захваченными астероидами[75].";
        }
        if(mercuryKey)
        {
            planetName = "Меркурий";
            description = "Меркурий (0,4 а. е. от Солнца) является ближайшей планетой к Солнцу и наименьшей планетой системы (0,055 массы Земли). У Меркурия нет спутников. Характерными деталями рельефа его поверхности, помимо ударных кратеров, являются многочисленные лопастевидные уступы, простирающиеся на сотни километров. Считается, что они возникли в результате приливных деформаций на раннем этапе истории планеты во время, когда периоды обращения Меркурия вокруг оси и вокруг Солнца не вошли в резонанс[63]. Меркурий имеет крайне разреженную атмосферу, она состоит из атомов, «выбитых» с поверхности планеты солнечным ветром[64]. Относительно большое железное ядро Меркурия и его тонкая кора ещё не получили удовлетворительного объяснения. Имеется гипотеза, предполагающая, что внешние слои планеты, состоящие из лёгких элементов, были сорваны в результате гигантского столкновения, которое уменьшило размеры планеты, а также предотвратило полное поглощение Меркурия молодым Солнцем[65][66].";
        }
        if(plutoKey)
        {
            planetName = "Плутон";
            description = "лутон — карликовая планета, крупнейший известный объект пояса Койпера. После обнаружения в 1930 году считался девятой планетой; положение изменилось в 2006 году с принятием формального определения планеты. У Плутона умеренный эксцентриситет орбиты с наклонением в 17 градусов к плоскости эклиптики, и он то приближается к Солнцу на расстояние 29,6 а. е., оказываясь к нему ближе Нептуна, то удаляется на 49,3 а. е. Неясна ситуация с крупнейшим спутником Плутона — Хароном: продолжит ли он классифицироваться как спутник Плутона или будет переклассифицирован в карликовую планету. Поскольку центр масс системы Плутон — Харон находится вне их поверхностей, они могут рассматриваться в качестве двойной планетной системы. Четыре меньших спутника — Никта, Гидра, Кербер и Стикс — обращаются вокруг Плутона и Харона. Плутон находится с Нептуном в орбитальном резонансе 3:2 — на каждые три оборота Нептуна вокруг Солнца приходится два оборота Плутона, весь цикл занимает 500 лет. Объекты пояса Койпера, чьи орбиты обладают таким же резонансом, называют плутино[103].";
        }
        if(jupiterKey)
        {
            planetName = "Юпитер";
            description = "Юпитер обладает массой в 318 раз больше земной, то есть в 2,5 раза массивнее всех остальных планет, вместе взятых. Он состоит главным образом из водорода и гелия. Высокая внутренняя температура Юпитера вызывает множество полупостоянных вихревых структур в его атмосфере, таких как полосы облаков и Большое красное пятно. У Юпитера имеется 67 спутников. Четыре крупнейших — Ганимед, Каллисто, Ио и Европа — схожи с планетами земной группы такими явлениями, как вулканическая активность и внутренний нагрев[86]. Ганимед, крупнейший спутник в Солнечной системе, больше Меркурия.";
        }
        if(neptuneKey)
        {
            planetName = "Нептун";
            description = "Нептун, хотя и немного меньше Урана, более массивен (17 масс Земли) и поэтому более плотный. Он излучает больше внутреннего тепла, но не так много, как Юпитер или Сатурн[90]. У Нептуна имеется 14 известных спутников. Крупнейший — Тритон, является геологически активным, с гейзерами жидкого азота[91]. Тритон — единственный крупный спутник, движущийся в обратном направлении. Также Нептун сопровождается астероидами, называемыми троянцы Нептуна, которые находятся с ним в резонансе 1:1.";
        }
        if(saturnKey)
        {
            planetName = "Сатурн";
            description = "Сатурн, известный своей обширной системой колец, имеет несколько схожие с Юпитером структуру атмосферы и магнитосферы. Хотя размер Сатурна составляет 60 % юпитерианского, масса (95 масс Земли) — меньше трети юпитерианской; таким образом, Сатурн — наименее плотная планета Солнечной системы (его средняя плотность сравнима с плотностью воды). У Сатурна имеется 62 подтверждённых спутника; два из них — Титан и Энцелад — проявляют признаки геологической активности. Активность эта, однако, не схожа с земной, поскольку в значительной степени обусловлена активностью льда[87]. Титан, превосходящий размерами Меркурий, — единственный спутник в Солнечной системе с существенной атмосферой.";
        }
        if(uranusKey)
        {
            planetName = "Уран";
            description = "Уран с массой в 14 раз больше, чем у Земли, является самой лёгкой из внешних планет. Уникальным среди других планет его делает то, что он вращается «лёжа на боку»; наклон оси его вращения к плоскости эклиптики равен примерно 98°[88]. Если другие планеты можно сравнить с вращающимися волчками, то Уран больше похож на катящийся шар. Он имеет намного более холодное ядро, чем другие газовые гиганты, и излучает очень немного тепла в космос[89]. У Урана открыты 27 спутников; крупнейшие — Титания, Оберон, Умбриэль, Ариэль и Миранда.";
        }
        if(venusKey)
        {
            planetName = "Венера";
            description = "Венера близка по размеру к Земле (0,815 земной массы) и, как и Земля, имеет толстую силикатную оболочку вокруг железного ядра и атмосферу. Имеются также свидетельства её внутренней геологической активности. Однако количество воды на Венере гораздо меньше земного, а её атмосфера в девяносто раз плотнее. У Венеры нет спутников. Это самая горячая планета нашей системы, температура её поверхности превышает 400 °C. Наиболее вероятной причиной столь высокой температуры является парниковый эффект, возникающий из-за плотной атмосферы, богатой углекислым газом[67]. Не было обнаружено никаких однозначных свидетельств геологической деятельности на Венере, но, так как у неё нет магнитного поля, которое предотвратило бы истощение её существенной атмосферы, это позволяет допустить, что её атмосфера регулярно пополняется вулканическими извержениями[68].";
        }
        if(moonKey)
        {
            planetName = "Луна";
            description = "Луна́ — естественный спутник Земли. Второй по яркости[комм. 1] объект на земном небосводе после Солнца и пятый по величине естественный спутник планеты Солнечной системы. Среднее расстояние между центрами Земли и Луны — 384 467 км (0,002 57 а. е., ~ 30 диаметров Земли). Видимая звёздная величина полной Луны на земном небе −12,71m[3]. Освещённость, создаваемая полной Луной возле поверхности Земли при ясной погоде, составляет 0,25 — 1 лк. Луна является единственным астрономическим объектом вне Земли, на котором побывал человек.";
        }
        if(calistoKey)
        {
            planetName = "Каллисто";
            description = "Каллисто — второй по размеру спутник Юпитера, один из четырёх галилеевых спутников и самый далёкий от планеты среди них[2] Был открыт в 1610 году Галилео Галилеем, назван в честь персонажа древнегреческой мифологии — Каллисто (греч. Καλλιστώ), любовницы Зевса. Благодаря низкому уровню радиационного фона в окрестностях Каллисто и её размерам её часто предлагают для основания станции, которая послужит для дальнейшего освоения системы Юпитера человечеством[8]. На 2012 год основной объём знаний об этом спутнике получен аппаратом «Галилео»; другие АМС — «Пионер-10», «Пионер-11», «Вояджер-1», «Вояджер-2», «Кассини» и «Новые горизонты» — изучали спутник во время полёта к другим объектам.";
        }
        if(europaKey)
        {
            planetName = "Европа";
            description = "Евро́па (др.-греч. Ἐυρώπη), или Юпитер II — шестой спутник Юпитера, наименьший из четырёх галилеевых спутников, один из самых крупных спутников в Солнечной системе. Обнаружена в 1610 году Галилео Галилеем[1] и, вероятно, Симоном Марием в то же самое время. На протяжении столетий за Европой велись всё более всесторонние наблюдения при помощи телескопов, а также, начиная с 1970-х годов, пролетающих вблизи космических аппаратов. По размерам уступая земной Луне, Европа состоит из силикатных пород, а в центре спутника находится железное ядро. Поверхность состоит изо льда и является одной из самых гладких в Солнечной системе; она испещрена поперечно-полосатыми трещинами и полосами, в то время как кратеров практически нет. Легко заметная молодость и гладкость поверхности привели к гипотезе, что на Европе находится подповерхностный океан, состоящий из воды, который может служить пристанищем для внеземной микробиологической жизни[4]. Гипотеза образования океана сводится к тому, что тепловая энергия от приливного ускорения позволила ему оставаться жидким и стимулировала эндогенную геологическую активность, близкую к тектонике плит[5]. У спутника есть крайне разрежённая атмосфера, состоящая в основном из кислорода. Интересные характеристики Европы, особенно возможность обнаружения внеземной жизни, привели к целому ряду предложений по исследованиям спутника[6][7]. Миссия КА «Галилео», начавшаяся в 1989 году, предоставила большую часть текущих данных о Европе. Запуск новой миссии по изучению ледяных спутников Юпитера, Jupiter Icy Moon Explorer (JUICE), запланирован на 2022 год[8].";
        }
        if(ganymedeKey)
        {
            planetName = "Ганимед";
            description = "Ганимед (др.-греч. Γανυμήδης) — один из галилеевых спутников Юпитера, седьмой по расстоянию от него среди всех его спутников[12] и крупнейший спутник в Солнечной системе. Его диаметр равен 5268 километров, что на 2 % больше, чем у Титана (второго по величине спутника в Солнечной системе) и на 8 % больше, чем у Меркурия. При этом масса Ганимеда составляет всего 45 % массы Меркурия, но среди спутников планет она рекордная. Луну Ганимед превышает по массе в 2,02 раза[13][14]. Совершая облёт орбиты примерно за семь дней, Ганимед участвует в орбитальном резонансе 1:2:4 с двумя другими спутниками Юпитера — Европой и Ио. Ганимед состоит из примерно равного количества силикатных пород и водяного льда. Это полностью дифференцированное тело с жидким ядром, богатым железом. Предположительно в его недрах на глубине около 200 км между слоями льда есть океан жидкой воды[15]. На поверхности Ганимеда наблюдаются два типа ландшафта. Треть поверхности спутника занимают тёмные области, испещрённые ударными кратерами. Их возраст доходит до четырёх миллиардов лет. Остальную площадь занимают более молодые светлые области, покрытые бороздами и хребтами. Причины сложной геологии светлых областей понятны не до конца. Вероятно, она связана с тектонической активностью, вызванной приливным нагревом[5]. Ганимед — единственный спутник в Солнечной системе, обладающий собственной магнитосферой. Скорее всего, её создаёт конвекция в жидком ядре, богатом железом[16]. Небольшая магнитосфера Ганимеда заключена в пределах намного большей магнитосферы Юпитера и лишь немного деформирует её силовые линии. У спутника есть тонкая атмосфера, в состав которой входит кислород в виде O, O2 и, возможно, O3 (озон)[11]. Количество атомарного водорода в атмосфере незначительно. Есть ли у Ганимеда ионосфера, неясно[17]. Честь открытия Ганимеда принадлежит Галилео Галилею, который увидел его 7 января 1610 года[1][2][3]. Вскоре Симон Марий предложил назвать его в честь Ганимеда — виночерпия на пирах богов и, по некоторым данным, любовника Зевса[18]. Первым космическим аппаратом, изучавшим Ганимед, стал «Пионер-10» в 1973 году[19]. Намного более детальные исследования провели аппараты программы «Вояджер» в 1979 году. Космический аппарат «Галилео», изучавший систему Юпитера начиная с 1995 года, обнаружил подземный океан и магнитное поле Ганимеда. В 2012 году Европейское космическое агентство одобрило новую миссию для исследований ледяных спутников Юпитера — JUICE; её запуск планируется на 2022 год, а прибытие в систему Юпитера — на 2030 год. На 2020 год запланирована миссия Europa Jupiter System Mission, составной частью которой, возможно, станет российский посадочный модуль «Лаплас»[20].";
        }
        if(loKey)
        {
            planetName = "Ио";
            description = "Ио́ (др.-греч. Ἰώ) — спутник Юпитера, самый близкий к планете из четырёх галилеевых спутников. Имеет диаметр 3 642 километра, что делает её четвёртым по величине спутником в Солнечной системе. Назван в честь мифологической Ио — жрицы Геры и возлюбленной Зевса. На Ио находится более 400 действующих вулканов, благодаря которым этот спутник является наиболее геологически активным во всей Солнечной системе[6][7]. Эта чрезвычайная геологическая активность обусловлена периодическим нагревом недр спутника в результате трения, которое происходит, скорее всего, из-за приливных гравитационных воздействий со стороны Юпитера, Европы и Ганимеда. У некоторых вулканов выбросы серы и диоксида серы настолько сильны, что поднимаются на высоту 500 километров. На поверхности Ио можно заметить более 100 гор, которые выросли благодаря сжатию в основании силикатной коры спутника. Некоторые из этих пиков выше горы Эверест на Земле[8]. В отличие от большинства спутников во внешней части Солнечной системы (которые в основном состоят из водяного льда), Ио в основном состоит из силикатных пород, окружающих расплавленное ядро из железа или сернистого железа. На большей части поверхности Ио простираются обширные равнины, покрытые замороженной серой или диоксидом серы. Вулканизм придаёт поверхности Ио уникальные особенности. Вулканический пепел и потоки лавы постоянно изменяют поверхность и окрашивают её в различные оттенки жёлтого, белого, красного, чёрного и зелёного (во многом благодаря аллотропам и соединениям серы). Потоки лавы на Ио достигают длины 500 километров. Вулканические выбросы создают тонкую неоднородную атмосферу Ио и потоки плазмы в магнитосфере Юпитера, в том числе огромный плазменный тор вокруг него. Ио сыграла значительную роль в развитии астрономии 17—18 веков. Её, вместе с другими галилеевыми спутниками, открыл Галилео Галилей в 1610 году. Это открытие способствовало принятию модели Солнечной системы Коперника, разработке законов движения планет Кеплера и первому измерению скорости света. Ио наблюдали только как яркую точку вплоть до конца 19-го — начала 20-го века, когда стало возможным рассмотреть самые большие детали её поверхности — тёмно-красный полярный и светлый экваториальный районы. В 1979 году два космических корабля «Вояджер» представили Ио миру как геологически активный спутник с многочисленными вулканами, большими горами и сравнительно молодой поверхностью без каких-либо заметных ударных кратеров. Космический аппарат «Галилео» выполнил несколько близких пролётов в 1990-х и в начале 2000-х годов, получив данные о внутренней структуре и составе поверхности Ио. Эти космические корабли также обнаружили связь между спутником и магнитосферой Юпитера и радиационный пояс вдоль орбиты Ио. Ио получает около 3600 бэр (36 Зв) радиации в день[9]. В дальнейшем Ио наблюдали космический аппарат «Кассини-Гюйгенс» в 2000 году и космическая межпланетная станция «Новые горизонты» в 2007 году, а также, благодаря развитию технологий, наземные телескопы и космический телескоп «Хаббл».";
        }
        if(demiosKey)
        {
            planetName = "Деймос";
            description = "Де́ймос (греч. Δείμος «ужас») — один из двух спутников Марса. Был открыт американским астрономом Асафом Холлом в 1877 году и назван им в честь древнегреческого бога ужаса Деймоса, спутника бога войны Ареса. Диаметр Деймоса порядка 13 км, обращается он на среднем расстоянии 6,96 радиуса планеты (примерно 23 500 км), с периодом обращения в 30 ч 17 мин 55 с. Он имеет почти круговую орбиту, вследствие чего пери- и апоцентр различаются всего на 10 км (± 5 км от большой полуоси). У Деймоса, как и Луны, угловая скорость движения по орбите равна угловой скорости собственного вращения, поэтому он всегда повернут к Марсу одной и той же стороной.";
        }
        if(phobosKey)
        {
            planetName = "Фобос";
            description = "Фобос обращается на среднем расстоянии 2,77 радиуса Марса от центра планеты (9400 км), что в 40 раз меньше, чем расстояние от Земли до Луны(356000 км), перицентр составляет 9235,6 км, апоцентр — 9518,8 км. Он делает один оборот за 7 ч 39 мин 14 с, что примерно в три раза быстрее вращения Марса вокруг собственной оси. В результате на марсианском небе Фобос восходит на западе и заходит на востоке. Размеры Фобоса составляют 27 × 22 × 18 км. Вследствие крайне малой массы атмосфера у Фобоса отсутствует. Чрезвычайно низкая средняя плотность Фобоса — около 1,86 г/см³, указывает на пористую структуру спутника с пустотами, составляющими 25—45 % объёма[2] . Период вращения Фобоса вокруг своей оси совпадает с периодом его обращения вокруг Марса, поэтому Фобос всегда повернут к планете одной и той же стороной. Его орбита находится внутри предела Роша, и спутник не разрывается только за счёт своей прочности[источник не указан 564 дня]. Такое расположение орбиты приводит к тому, что с Фобоса срываются камни, часто оставляющие заметные борозды на поверхности спутника. Приливное воздействие Марса постепенно замедляет движение Фобоса и в будущем приведёт к его падению на Марс. Согласно расчетам такое событие произойдет через 11 миллионов лет[3], хотя другие расчеты указывают на то, что Фобос разрушится на многие куски уже через 7,6 миллиона лет[4]. Каждые 100 лет Фобос приближается к Марсу на 9 см.";
        }
        if(dionaKey)
        {
            planetName = "Диона";
            description = "Диону открыл Джованни Кассини в 1684. Он назвал 4 открытых им спутника Сатурна «звёздами Людовика» (лат. Sidera Lodoicea) в честь короля Франции Людовика XIV. Астрономы долгое время обозначали Диону как «четвёртый спутник Сатурна» (Saturn IV). Современное название спутника предложил Джон Гершель (сын Вильяма Гершеля) в 1847. Он выдвинул идею[1] назвать семь известных на тот момент спутников Сатурна именами титанов — братьев и сестёр Кроноса (аналога Сатурна в греческой мифологии).";
        }
        if(enceladusKey)
        {
            planetName = "Энцелад";
            description = "Энцела́д — шестой по размеру спутник Сатурна[10]. Был открыт ещё в 1789 году Уильямом Гершелем[11], но оставался малоизученным до начала 1980-х, когда с ним сблизились два межпланетных зонда «Вояджер». Их снимки позволили определить его диаметр (около 500 км, или 0,1 от диаметра крупнейшего спутника Сатурна — Титана) и обнаружить, что поверхность Энцелада отражает почти весь падающий на неё солнечный свет. «Вояджер-1» показал, что орбита спутника проходит по наиболее плотной части рассеянного кольца Е и обменивается с ним веществом; по-видимому, это кольцо обязано Энцеладу своим происхождением. «Вояджер-2» обнаружил, что рельеф поверхности этого небольшого спутника очень разнообразен: там есть и старые сильно кратерированные области, и молодые участки (возраст некоторых не превышает 100 млн лет). В 2005 году изучение Энцелада начал межпланетный зонд «Кассини», который получил более подробные данные о поверхности спутника и происходящих на ней процессах. В частности, был открыт богатый водой шлейф, фонтанирующий из южной полярной области (вероятно, такие ледяные фонтаны и сформировали кольцо E). Это открытие, наряду с признаками наличия внутреннего тепла и малым числом ударных кратеров в области южного полюса, указывает на то, что геологическая активность на Энцеладе сохраняется по сей день. Спутники в обширных спутниковых системах газовых гигантов часто попадают в ловушку орбитальных резонансов, которые поддерживают сильные либрации или большой эксцентриситет орбиты; у близких к планете спутников это может вызвать периодическое нагревание недр, что в принципе может объяснять геологическую активность. Энцелад — одно из трёх небесных тел во внешней Солнечной системе (наряду со спутником Юпитера Ио и спутником Нептуна Тритоном), на которых наблюдались активные извержения. Анализ выбросов указывает на то, что они выбиваются из подповерхностного жидкого водного океана. Вместе с уникальным химическим составом шлейфа это служит основой для предположений о важности Энцелада для астробиологических исследований[12]. Открытие шлейфа, помимо прочего, добавило веса к аргументам в пользу того, что Энцелад — источник материи кольца Сатурна Е. В 2011 году учёные NASA на «Enceladus Focus Group Conference» заявили, что Энцелад — «наиболее пригодное для такой жизни, какую мы знаем, место в Солнечной системе за пределами Земли»[13][14]. Астробиолог Крис Маккей из Исследовательского центра NASA в Эймсе в 2011 году заявил, что в Солнечной системе только на Энцеладе обнаружены «жидкая вода, углерод, азот в форме аммиака и источник энергии»[15].";
        }
        if(mimasKey)
        {
            planetName = "Мимас";
            description = "Ми́мас — спутник Сатурна, открытый 17 сентября 1789 года Уильямом Гершелем. Назван в честь Мимаса, сына Геи из греческой мифологии. Имея размер около 400 километров, является двадцатым по величине спутником в Солнечной системе. Мимас является самым маленьким известным астрономическим телом, которое имеет округлую форму из-за собственной гравитации. Возможно, что Мимас является минимально допустимым по форме, размеру и массе телом, для зачисления его в карликовые планеты, если бы он не являлся спутником.";
        }
        if(rheaKey)
        {
            planetName = "Рея";
            description = "Рея — ледяное тело со средней плотностью, равной 1233 кг/м³[3]. Столь низкая плотность свидетельствует, что каменные породы составляют менее трети массы спутника, а остальное приходится на водяной лёд. Ускорение свободного падения составляет 0,264 м/с². Размеры спутника составляют 1532,4×1525,6×1524,4 км.";
        }
        if(tehysKey)
        {
            planetName = "Тефия";
            description = "Те́фия (Те́тис) (др.-греч. Τηθύς) — спутник Сатурна средней величины, его диаметр составляет около 1060 км. Тефия была открыта Д. Кассини в 1684 году и получила имя одного из титанов греческой мифологии. Видимая звёздная величина Тефии составляет 10,2[9]. Этот спутник известен также в русской транскрипции как Тетис и Тефида. Тефия имеет сравнительно низкую плотность (0,98 г/см³), что указывает на то, что она состоит преимущественно из водяного льда с малой примесью пород. Спектроскопические замеры показали, что практически вся её поверхность состоит из льда. Так же на Тефии присутствует небольшое количество тёмного вещества неизвестного состава. Поверхность Тефии очень светлая и у неё нейтральный показатель цвета, она является второй по показателю альбедо из спутников Сатурна, после Энцелада. Тефия сильно кратерирована и имеет ряд крупных структур на своей поверхности. Крупнейшим кратером на Тефии является Одиссей, диаметр которого около 400 км, крупнейшим рифт — каньон Итака имеет длину более 2000 км при ширине около 100 км. Эти две крупнейшие детали поверхности могут быть связаны между собой. Небольшая часть поверхности покрыта гладкой равниной, которая могла образоваться вследствие криовулканической активности. Как и другие регулярные спутники Сатурна, Тефия сформировалась к югу от диска туманности из газа и пыли, окружавшей Сатурн сразу после его образования. Тефия была изучена КА Пионер-11 (1979 год), Вояджер-1 (1980), Вояджер-2 (1981) и АМС Кассини в 2004 году, пролетавших вблизи неё. Находится в орбитальном резонансе с троянскими спутниками — Телесто и Калипсо.";
        }
        if(titanKey)
        {
            planetName = "Титан";
            description = "Тита́н (др.-греч. Τιτάν) — крупнейший спутник Сатурна, второй по величине спутник в Солнечной системе (после спутника Юпитера Ганимеда), является единственным, кроме Земли, телом в Солнечной системе, для которого доказано существование жидкости на поверхности[7][8], и единственным спутником планеты, обладающим плотной атмосферой. Исследования Титана позволили выдвинуть гипотезу о наличии на нём примитивных форм жизни[9]. Титан стал первым известным спутником Сатурна — в 1655 году его обнаружил голландский астроном Христиан Гюйгенс[10]. Диаметр Титана — 5152 км, это на 50 % больше, чем у Луны, при этом Титан на 80 % превосходит спутник Земли по массе. Титан также превосходит размерами планету Меркурий, хотя и уступает ей по массе. Сила тяжести на нём составляет приблизительно одну седьмую земной. Масса Титана составляет 95 % массы всех спутников Сатурна. Поверхность Титана в основном состоит из водяного льда и осадочных органических веществ, геологически молодая, в основном ровная, за исключением небольшого количества горных образований и кратеров, а также нескольких криовулканов. Плотная атмосфера, окружающая Титан, долгое время не позволяла увидеть поверхность спутника вплоть до прибытия аппарата «Кассини — Гюйгенс» в 2005 году. Атмосфера преимущественно состоит из азота, также имеется небольшое количество метана и этана, которые образуют облака, являющиеся источником жидких и, возможно, твёрдых осадков. На поверхности имеются метан-этановые озёра и реки. Давление у поверхности примерно в 1,5 раза превышает давление земной атмосферы. Температура у поверхности — минус 170—180 °C. Несмотря на низкую температуру, Титан сопоставляется с Землёй на ранних стадиях развития, и нельзя исключать, что на спутнике возможно существование простейших форм жизни, в частности, в подземных водоёмах, где условия могут быть гораздо комфортнее, чем на поверхности[11][12].";
        }
        
        GUI.Box(Rect(screenRightSide,Screen.height/2 - 330*screenScale,Screen.width/2 - 20,660*screenScale),"");
        leftStyle.alignment = TextAnchor.UpperLeft;
        //    			leftStyle.font = skin.textField.font;
        leftStyle.fontSize = 28.0f;
        var height : float = leftStyle.CalcHeight(GUIContent(description), Screen.width/2 - 60);
        scrollPosition = GUI.BeginScrollView(Rect(screenRightSide + 20,Screen.height/2 - 330*screenScale + 70.0,Screen.width/2 - 60,600*screenScale),scrollPosition,Rect(0,0,Screen.width/2 - 60,height));
        GUI.Label(Rect(0,0,Screen.width/2 - 60,height),description,leftStyle);
        GUI.EndScrollView();
        rightStyle.alignment = TextAnchor.MiddleRight;
        GUI.Label(Rect(screenRightSide + Screen.width/2 - 430,Screen.height/2 - 330*screenScale,280,60),planetName,rightStyle);
        if(GUI.Button(Rect(screenRightSide + Screen.width/2 - 80,Screen.height/2 - 330*screenScale,60,60),"x"))
        {
            moveBack = true;
            info = false;
        }
    }
    if(satelites && !moveBack)
    {
        showingDetails = true;
        rightStyle = GUI.skin.GetStyle("Label");
        leftStyle = GUI.skin.GetStyle("Label");
        GUI.Box(Rect(screenRightSide,Screen.height/2 - 350*screenScale,Screen.width/2 - 20,700*screenScale),"");
        leftStyle.alignment = TextAnchor.UpperLeft;
        GUI.Label(Rect(screenRightSide + 20,Screen.height/2 - 350*screenScale + 70.0,Screen.width/2 - 60,620*screenScale),"some text jbiaewbiwebvwieuvbweivbuweivbweivbweivuabibudsiduvhisduvhaidshvadiuhvaidsuhvuisdohvdisuvhiudshvaisduhvdiuhvasiduhvidsuhvaiosvhuaodisvhuisvaosidhvsdjvavdvad",leftStyle);
        rightStyle.alignment = TextAnchor.MiddleRight;
        GUI.Label(Rect(screenRightSide + Screen.width/2 - 430,Screen.height/2 - 350*screenScale,280,60),"Satelites info",rightStyle);
        if(GUI.Button(Rect(screenRightSide + Screen.width/2 - 80,Screen.height/2 - 350*screenScale,60,60),"x"))
        {
            moveBack = true;
            satelites = false;
        }
    }
    if(images && !moveBack)
    {
        showingDetails = true;
        GUI.Box(Rect(screenRightSide,Screen.height/2 - 350*screenScale,Screen.width/2 - 20,700*screenScale),"");
        //place pictures here
        var array = Array();
        if(sunKey)
        {
            //set an array of images names
            array = ["testImage"];
        }
        if(earthKey)
        {
            array = ["testImage"];
        }
        if(marsKey)
        {
            array = ["testImage"];
        }
        if(mercuryKey)
        {
            array = ["testImage"];
        }
        if(plutoKey)
        {
            array = ["testImage"];
        }
        if(jupiterKey)
        {
            array = ["testImage"];
        }
        if(neptuneKey)
        {
            array = ["testImage"];
        }
        if(saturnKey)
        {
            array = ["testImage"];
        }
        if(uranusKey)
        {
            array = ["testImage"];
        }
        if(venusKey)
        {
            array = ["testImage"];
        }
        if(moonKey)
        {
            array = ["testImage"];
        }
        if(calistoKey)
        {
            array = ["testImage"];
        }
        if(europaKey)
        {
            array = ["testImage"];
        }
        if(ganymedeKey)
        {
            array = ["testImage"];
        }
        if(loKey)
        {
            array = ["testImage"];
        }
        if(demiosKey)
        {
            array = ["testImage"];
        }
        if(phobosKey)
        {
            array = ["testImage"];
        }
        if(dionaKey)
        {
            array = ["testImage"];
        }
        if(enceladusKey)
        {
            array = ["testImage"];
        }
        if(mimasKey)
        {
            array = ["testImage"];
        }
        if(rheaKey)
        {
            array = ["testImage"];
        }
        if(tehysKey)
        {
            array = ["testImage"];
        }
        if(titanKey)
        {
            array = ["testImage"];
        }
        //create a loop for images placement
        var n : int = array.Count;
        for(var i : int = 0;i < n;i+=2)
        {
            var name : String = array[i] as String;
            var texture : Texture2D = Resources.Load(name) as Texture2D;
            GUI.DrawTexture(Rect(screenRightSide + 20,Screen.height/2 - 350*screenScale + 70.0,240,240),texture, ScaleMode.ScaleToFit);
            GUI.DrawTexture(Rect(screenRightSide + 280,Screen.height/2 - 350*screenScale + 70.0,240,240),texture, ScaleMode.ScaleToFit);
        }
        if(GUI.Button(Rect(screenRightSide + Screen.width/2 - 80,Screen.height/2 - 350*screenScale,60,60),"x"))
        {
            moveBack = true;
            images = false;
        }
    }
}

function Start ()
{
    serverCtrl = Camera.main.GetComponent(ServerController);
    var GUIT : GUITexture = joy.GetComponent(GUITexture);
    joy.GetComponent(GUITexture).pixelInset.x = Screen.width/2 - 40.0;
    joy.Find("bg").GetComponent(GUITexture).pixelInset.x = Screen.width/2 - 95.0;
    
	//textures = Resources.LoadAll("Resources",Texture2D);
	sl = Camera.main.GetComponent(sliderControl);
    
    mars = GameObject.Find("mars");
    earth = GameObject.Find("earth");
    mercury = GameObject.Find("mercury");
    sun = GameObject.Find("Sun");
    jupiter = GameObject.Find("jupiter");
    neptune = GameObject.Find("neptune");
    pluto = GameObject.Find("pluto");
    saturn = GameObject.Find("saturn");
    uranus = GameObject.Find("uranus");
    venus = GameObject.Find("venus");
    //satelites
    moon = GameObject.Find("moon");
    calisto = GameObject.Find("calisto");
    europa = GameObject.Find("europa");
    ganymede = GameObject.Find("ganymede");
    lo = GameObject.Find("lo");
    demios = GameObject.Find("demios");
    phobos = GameObject.Find("phobos");
    diona = GameObject.Find("diona");
    enceladus = GameObject.Find("enceladus");
    mimas = GameObject.Find("mimas");
    rhea = GameObject.Find("rhea");
    tehys = GameObject.Find("tehys");
    titan = GameObject.Find("titan");
}

function Update ()
{
    if(effect)
    {
        if(index <= 275)
        {
            index = index+1;
        }
        else index = 37;
        if(index >36 && indexForMenu<=90)
        {
            indexForMenu = indexForMenu+1;
        }
        else indexForMenu = 39;
        
    }
    if(!follow && sl.sliderValue > 0.9)
    {
        //planets -----
        sunKey = false;
        marsKey = false;
        earthKey = false;
        mercuryKey = false;
        jupiterKey = false;
        neptuneKey = false;
        plutoKey = false;
        saturnKey = false;
        uranusKey = false;
        venusKey = false;
        //satelites -----
        moonKey = false;
        calistoKey = false;
        europaKey = false;
        ganymedeKey = false;
        loKey = false;
        demiosKey = false;
        phobosKey = false;
        dionaKey = false;
        enceladusKey = false;
        mimasKey = false;
        rheaKey = false;
        tehysKey = false;
        titanKey = false;
        //----------------
        menu = false;
        drawGUIforPlanet = false;
        showingDetails = false;
        info = false;
        satelites = false;
        images = false;
        if(target && target.audio.isPlaying == true)
		{
			target.audio.Stop();
		}
        if(target)
		{
			var s : MP = target.Find("movie").GetComponent(MP);
			s.playMovie = false;
		}
        target = null;
        iTween.MoveTo(transform.gameObject,new Vector3(-89.5,70.9,-221.3),15);
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(21.5,17.6,5.9), Time.deltaTime * 10.0);
    }
    if((screenRightSide > Screen.width/2 && (info || satelites || images)) && !moveBack)
    {
        screenRightSide = screenRightSide - Screen.width*Time.deltaTime;
        menuPoint = menuPoint + Screen.width*Time.deltaTime;
    }
    //rotation by joystick and movement
    var joystickRot : Joystick = joy.GetComponent(Joystick);
    if (target && follow)
    {
        drawGUIforPlanet = false;
        var rotationInFollow = Quaternion.LookRotation(target.transform.position - transform.position);
        range = Vector3.Distance(transform.position,target.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotationInFollow, Time.deltaTime * 20.0);
        var vector : Vector3 =  new Vector3(target.position.x,target.position.y+(distance*Mathf.Sin(0.2618)),target.position.z - distance*Mathf.Cos(0.2618));
        iTween.MoveTo(transform.gameObject,iTween.Hash("position",vector,"easetype",iTween.EaseType.easeInOutSine,"time",5.0f));
        if(vector == transform.position)//(range-0.5 <= (distance * Mathf.Cos(0.2618)))
        {
            var angles = transform.eulerAngles;
            x = angles.y;
            y = angles.x;
            follow = false;
            moveBack = true;
            rotateAround = true;
        }
    }
    //rotation
    else if(rotateAround && !moveBack)//(target && !follow && !showingDetails && !moveBack)
    {
        x += joystickRot.position.x * xSpeed * 0.02;
        y -= joystickRot.position.y * ySpeed * 0.02;
 		
 		y = ClampAngle(y, yMinLimit, yMaxLimit);
        
        var rotation = Quaternion.Euler(y, x, 0);
        var position = rotation * Vector3(0.0, 0.0,  -distance*Mathf.Cos(0.2618)) + target.position;
        transform.rotation = rotation;
        transform.position = position;
    }
    if((info || images || satelites) && showingDetails)
    {
        //rotate to angel
        var p = transform.rotation;
        var step = distance * Mathf.Tan(0.2618);
        var p2 = p * Vector3(step, 0.0,  -distance*Mathf.Cos(0.2618)) + target.position;
        transform.rotation = p;
        iTween.MoveTo(transform.gameObject,iTween.Hash("position",p2,"easetype",iTween.EaseType.easeInOutSine,"time",0.5f));
        showingDetails = false;
    }
    if(moveBack)
    {
        drawGUIforPlanet = true;
        //rotate to angel
        var pBack = transform.rotation;
        var p2Back = pBack * Vector3(0.0, 0.0, -distance*Mathf.Cos(0.2618)) + target.position;
        transform.rotation = pBack;
        iTween.MoveTo(transform.gameObject,iTween.Hash("position",p2Back,"easetype",iTween.EaseType.easeInOutSine,"time",0.5f));
        if(screenRightSide < Screen.width)
        {
            screenRightSide = screenRightSide + Screen.width*Time.deltaTime;
            menuPoint = menuPoint - Screen.width*Time.deltaTime;
        }
        else
        {
            moveBack = false;
            showingDetails = false;
        }
    }
    //draw menu for planet
    if(target && (!scaleSmaller && !scaleBigger) && !follow && !showingDetails && sl.sliderValue == 0 && joystickRot.tapCount == 0 && tmpDistance == distance)
    {
        drawGUIforPlanet = true;
        effect = true;
    }
    else
    {
        drawGUIforPlanet = false;
        effect = false;
    }
    //animation of scale
    var v : Vector3 = new Vector3(0,0,0);
    if(scaleSmaller && !showingDetails && !follow)
    {
        if(sunKey)
        {
            if(sun.transform.localScale.x > 3 && sun.transform.localScale.y > 3 && sun.transform.localScale.z > 3)
            {
                v = sun.transform.localScale;
                sun.transform.localScale = new Vector3(v.x - 0.5,v.y - 0.5,v.z - 0.5);
            }
        }
        if(earthKey)
        {
            if(earth.transform.localScale.x > 1 && earth.transform.localScale.y > 1 && earth.transform.localScale.z > 1)
            {
                v = earth.transform.localScale;
                earth.transform.localScale = new Vector3(v.x - 0.5,v.y - 0.5,v.z - 0.5);
            }
        }
        if(marsKey)
        {
            if(mars.transform.localScale.x > 0.5 && mars.transform.localScale.y > 0.5 && mars.transform.localScale.z > 0.5)
            {
                v = mars.transform.localScale;
                mars.transform.localScale = new Vector3(v.x - 0.5,v.y - 0.5,v.z - 0.5);
            }
        }
        if(mercuryKey)
        {
            if(mercury.transform.localScale.x > 1 && mercury.transform.localScale.y > 1 && mercury.transform.localScale.z > 1)
            {
                v = mercury.transform.localScale;
                mercury.transform.localScale = new Vector3(v.x - 0.5,v.y - 0.5,v.z - 0.5);
            }
        }
        if(plutoKey)
        {
            if(pluto.transform.localScale.x > 0.5 && pluto.transform.localScale.y > 0.5 && pluto.transform.localScale.z > 0.5)
            {
                v = pluto.transform.localScale;
                pluto.transform.localScale = new Vector3(v.x - 0.5,v.y - 0.5,v.z - 0.5);
            }
        }
        if(jupiterKey)
        {
        	if(jupiter.transform.localScale.x > 2 && jupiter.transform.localScale.y > 2 && jupiter.transform.localScale.z > 2)
            {
                v = jupiter.transform.localScale;
                jupiter.transform.localScale = new Vector3(v.x - 0.5,v.y - 0.5,v.z - 0.5);
            }
        }
        if(neptuneKey)
        {
        	if(neptune.transform.localScale.x > 1 && neptune.transform.localScale.y > 1 && neptune.transform.localScale.z > 1)
            {
                v = neptune.transform.localScale;
                neptune.transform.localScale = new Vector3(v.x - 0.5,v.y - 0.5,v.z - 0.5);
            }
        }
        if(saturnKey)
        {
        	if(saturn.transform.localScale.x > 1.5 && saturn.transform.localScale.y > 1.5 && saturn.transform.localScale.z > 1.5)
            {
                v = saturn.transform.localScale;
                saturn.transform.localScale = new Vector3(v.x - 0.5,v.y - 0.5,v.z - 0.5);
            }
        }
        if(uranusKey)
        {
        	if(uranus.transform.localScale.x > 1 && uranus.transform.localScale.y > 1 && uranus.transform.localScale.z > 1)
            {
                v = uranus.transform.localScale;
                uranus.transform.localScale = new Vector3(v.x - 0.5,v.y - 0.5,v.z - 0.5);
            }
        }
        if(venusKey)
        {
        	if(venus.transform.localScale.x > 1 && venus.transform.localScale.y > 1 && venus.transform.localScale.z > 1)
            {
                v = venus.transform.localScale;
                venus.transform.localScale = new Vector3(v.x - 0.5,v.y - 0.5,v.z - 0.5);
            }
        }
        if(moonKey)
        {
            if(moon.transform.localScale.x > 0.2 && moon.transform.localScale.y > 0.2 && moon.transform.localScale.z > 0.2)
            {
                v = moon.transform.localScale;
                moon.transform.localScale = new Vector3(v.x - 0.1,v.y - 0.1,v.z - 0.1);
            }
        }
        if(calistoKey)
        {
            if(calisto.transform.localScale.x > 0.1 && calisto.transform.localScale.y > 0.1 && calisto.transform.localScale.z > 0.1)
            {
                v = calisto.transform.localScale;
                calisto.transform.localScale = new Vector3(v.x - 0.05,v.y - 0.05,v.z - 0.05);
            }
        }
        if(europaKey)
        {
            if(europa.transform.localScale.x > 0.1 && europa.transform.localScale.y > 0.1 && europa.transform.localScale.z > 0.1)
            {
                v = europa.transform.localScale;
                europa.transform.localScale = new Vector3(v.x - 0.05,v.y - 0.05,v.z - 0.05);
            }
        }
        if(ganymedeKey)
        {
            if(ganymede.transform.localScale.x > 0.15 && ganymede.transform.localScale.y > 0.15 && ganymede.transform.localScale.z > 0.15)
            {
                v = ganymede.transform.localScale;
                ganymede.transform.localScale = new Vector3(v.x - 0.05,v.y - 0.05,v.z - 0.05);
            }
        }
        if(loKey)
        {
            if(lo.transform.localScale.x > 0.15 && lo.transform.localScale.y > 0.15 && lo.transform.localScale.z > 0.15)
            {
                v = lo.transform.localScale;
                lo.transform.localScale = new Vector3(v.x - 0.05,v.y - 0.05,v.z - 0.05);
            }
        }
        if(demiosKey)
        {
            if(demios.transform.localScale.x > 0.08 && demios.transform.localScale.y > 0.08 && demios.transform.localScale.z > 0.08)
            {
                v = demios.transform.localScale;
                demios.transform.localScale = new Vector3(v.x - 0.01,v.y - 0.01,v.z - 0.01);
            }
        }
        if(phobosKey)
        {
            if(phobos.transform.localScale.x > 0.08 && phobos.transform.localScale.y > 0.08 && phobos.transform.localScale.z > 0.08)
            {
                v = phobos.transform.localScale;
                phobos.transform.localScale = new Vector3(v.x - 0.01,v.y - 0.01,v.z - 0.01);
            }
        }
        if(dionaKey)
        {
            if(diona.transform.localScale.x > 0.04 && diona.transform.localScale.y > 0.04 && diona.transform.localScale.z > 0.04)
            {
                v = diona.transform.localScale;
                diona.transform.localScale = new Vector3(v.x - 0.010,v.y - 0.010,v.z - 0.010);
            }
        }
        if(enceladusKey)
        {
            if(enceladus.transform.localScale.x > 0.03 && enceladus.transform.localScale.y > 0.03 && enceladus.transform.localScale.z > 0.03)
            {
                v = enceladus.transform.localScale;
                enceladus.transform.localScale = new Vector3(v.x - 0.01,v.y - 0.01,v.z - 0.01);
            }
        }
        if(mimasKey)
        {
            if(mimas.transform.localScale.x > 0.03 && mimas.transform.localScale.y > 0.03 && mimas.transform.localScale.z > 0.03)
            {
                v = mimas.transform.localScale;
                mimas.transform.localScale = new Vector3(v.x - 0.015,v.y - 0.015,v.z - 0.015);
            }
        }
        if(rheaKey)
        {
            if(rhea.transform.localScale.x > 0.07 && rhea.transform.localScale.y > 0.07 && rhea.transform.localScale.z > 0.07)
            {
                v = rhea.transform.localScale;
                rhea.transform.localScale = new Vector3(v.x - 0.01,v.y - 0.01,v.z - 0.01);
            }
        }
        if(tehysKey)
        {
            if(tehys.transform.localScale.x > 0.036 && tehys.transform.localScale.y > 0.036 && tehys.transform.localScale.z > 0.036)
            {
                v = tehys.transform.localScale;
                tehys.transform.localScale = new Vector3(v.x - 0.011,v.y - 0.011,v.z - 0.011);
            }
        }
        if(titanKey)
        {
            if(titan.transform.localScale.x > 0.17 && titan.transform.localScale.y > 0.17 && titan.transform.localScale.z > 0.17)
            {
                v = titan.transform.localScale;
                titan.transform.localScale = new Vector3(v.x - 0.02,v.y - 0.02,v.z - 0.02);
            }
        }
    }
    else if(scaleActual && !showingDetails && !follow)
    {
        if(sunKey)
        {
            if(sun.transform.localScale.x != 5 && sun.transform.localScale.y != 5 && sun.transform.localScale.z != 5)
            {
                v = sun.transform.localScale;
                if(sun.transform.localScale.x > 5)
                    sun.transform.localScale = new Vector3(v.x - 0.5,v.y - 0.5,v.z - 0.5);
                else  sun.transform.localScale = new Vector3(v.x + 0.5,v.y + 0.5,v.z + 0.5);
            }
            else scaleActual = false;
        }
        if(earthKey)
        {
            if(earth.transform.localScale.x != 2 && earth.transform.localScale.y != 2 && earth.transform.localScale.z != 2)
            {
                v = earth.transform.localScale;
                if(earth.transform.localScale.x > 2)
                    earth.transform.localScale = new Vector3(v.x - 0.5,v.y - 0.5,v.z - 0.5);
                else  earth.transform.localScale = new Vector3(v.x + 0.5,v.y + 0.5,v.z + 0.5);
            }
            else scaleActual = false;
        }
        if(marsKey)
        {
            if(mars.transform.localScale.x != 1 && mars.transform.localScale.y != 1 && mars.transform.localScale.z != 1)
            {
                v = mars.transform.localScale;
                if(mars.transform.localScale.x > 1)
                    mars.transform.localScale = new Vector3(v.x - 0.5,v.y - 0.5,v.z - 0.5);
                else  mars.transform.localScale = new Vector3(v.x + 0.5,v.y + 0.5,v.z + 0.5);
            }
            else scaleActual = false;
        }
        if(mercuryKey)
        {
            if(mercury.transform.localScale.x != 3 && mercury.transform.localScale.y != 3 && mercury.transform.localScale.z != 3)
            {
                v = mercury.transform.localScale;
                if(mercury.transform.localScale.x > 3)
                    mercury.transform.localScale = new Vector3(v.x - 0.5,v.y - 0.5,v.z - 0.5);
                else  mercury.transform.localScale = new Vector3(v.x + 0.5,v.y + 0.5,v.z + 0.5);
            }
            else scaleActual = false;
        }
        if(plutoKey)
        {
            if(pluto.transform.localScale.x != 1 && pluto.transform.localScale.y != 1 && pluto.transform.localScale.z != 1)
            {
                v = pluto.transform.localScale;
                if(pluto.transform.localScale.x > 1)
                    pluto.transform.localScale = new Vector3(v.x - 0.5,v.y - 0.5,v.z - 0.5);
                else  pluto.transform.localScale = new Vector3(v.x + 0.5,v.y + 0.5,v.z + 0.5);
            }
            else scaleActual = false;
            //pluto.transform.localScale = new Vector3(1,1,1);
        }
        if(jupiterKey)
        {
        	if(jupiter.transform.localScale.x != 4 && jupiter.transform.localScale.y != 4 && jupiter.transform.localScale.z != 4)
            {
                v = jupiter.transform.localScale;
                if(jupiter.transform.localScale.x > 4)
                    jupiter.transform.localScale = new Vector3(v.x - 0.5,v.y - 0.5,v.z - 0.5);
                else  jupiter.transform.localScale = new Vector3(v.x + 0.5,v.y + 0.5,v.z + 0.5);
            }
            else scaleActual = false;
    		//jupiter.transform.localScale = new Vector3(4,4,4);
        }
        if(neptuneKey)
        {
        	if(neptune.transform.localScale.x != 3 && neptune.transform.localScale.y != 3 && neptune.transform.localScale.z != 3)
            {
                v = neptune.transform.localScale;
                if(neptune.transform.localScale.x > 3)
                    neptune.transform.localScale = new Vector3(v.x - 0.5,v.y - 0.5,v.z - 0.5);
                else  neptune.transform.localScale = new Vector3(v.x + 0.5,v.y + 0.5,v.z + 0.5);
            }
            else scaleActual = false;
        	//neptune.transform.localScale = new Vector3(3,3,3);
        }
        if(saturnKey)
        {
        	if(saturn.transform.localScale.x != 3.5 && saturn.transform.localScale.y != 3.5 && saturn.transform.localScale.z != 3.5)
            {
                v = saturn.transform.localScale;
                if(saturn.transform.localScale.x > 3.5)
                    saturn.transform.localScale = new Vector3(v.x - 0.5,v.y - 0.5,v.z - 0.5);
                else  saturn.transform.localScale = new Vector3(v.x + 0.5,v.y + 0.5,v.z + 0.5);
            }
            else scaleActual = false;
        	//saturn.transform.localScale = new Vector3(3.5,3.5,3.5);
        }
        if(uranusKey)
        {
        	if(uranus.transform.localScale.x != 3 && uranus.transform.localScale.y != 3 && uranus.transform.localScale.z != 3)
            {
                v = uranus.transform.localScale;
                if(uranus.transform.localScale.x > 3)
                    uranus.transform.localScale = new Vector3(v.x - 0.5,v.y - 0.5,v.z - 0.5);
                else  uranus.transform.localScale = new Vector3(v.x + 0.5,v.y + 0.5,v.z + 0.5);
            }
            else scaleActual = false;
        	//uranus.transform.localScale = new Vector3(3,3,3);
        }
        if(venusKey)
        {
        	if(venus.transform.localScale.x != 2 && venus.transform.localScale.y != 2 && venus.transform.localScale.z != 2)
            {
                v = venus.transform.localScale;
                if(venus.transform.localScale.x > 2)
                    venus.transform.localScale = new Vector3(v.x - 0.5,v.y - 0.5,v.z - 0.5);
                else  venus.transform.localScale = new Vector3(v.x + 0.5,v.y + 0.5,v.z + 0.5);
            }
            else scaleActual = false;
        	//venus.transform.localScale = new Vector3(2,2,2);
        }
        if(moonKey)
        {
            if(moon.transform.localScale.x != 0.3 && moon.transform.localScale.y != 0.3 && moon.transform.localScale.z != 0.3)
            {
                v = moon.transform.localScale;
                if(moon.transform.localScale.x > 0.3)
                    moon.transform.localScale = new Vector3(v.x - 0.1,v.y - 0.1,v.z - 0.1);
                else moon.transform.localScale = new Vector3(v.x + 0.1,v.y + 0.1,v.z + 0.1);
            }
            else scaleActual = false;
        }
        if(calistoKey)
        {
            if(calisto.transform.localScale.x != 0.2 && calisto.transform.localScale.y != 0.2 && calisto.transform.localScale.z != 0.2)
            {
                v = calisto.transform.localScale;
                if(calisto.transform.localScale.x > 0.2)
                    calisto.transform.localScale = new Vector3(v.x - 0.05,v.y - 0.05,v.z - 0.05);
                else calisto.transform.localScale = new Vector3(v.x + 0.05,v.y + 0.05,v.z + 0.05);
            }
            else scaleActual = false;
        }
        if(europaKey)
        {
            if(europa.transform.localScale.x != 0.15 && europa.transform.localScale.y != 0.15 && europa.transform.localScale.z != 0.15)
            {
                v = europa.transform.localScale;
                if(europa.transform.localScale.x > 0.15)
                    europa.transform.localScale = new Vector3(v.x - 0.05,v.y - 0.05,v.z - 0.05);
                else europa.transform.localScale = new Vector3(v.x + 0.05,v.y + 0.05,v.z + 0.05);
            }
            else scaleActual = false;
        }
        if(ganymedeKey)
        {
            if(ganymede.transform.localScale.x != 0.25 && ganymede.transform.localScale.y != 0.25 && ganymede.transform.localScale.z != 0.25)
            {
                v = ganymede.transform.localScale;
                if(ganymede.transform.localScale.x > 0.25)
                    ganymede.transform.localScale = new Vector3(v.x - 0.05,v.y - 0.05,v.z - 0.05);
                else ganymede.transform.localScale = new Vector3(v.x + 0.05,v.y + 0.05,v.z + 0.05);
            }
            else scaleActual = false;
        }
        if(loKey)
        {
            if(lo.transform.localScale.x != 0.25 && lo.transform.localScale.y != 0.25 && lo.transform.localScale.z != 0.25)
            {
                v = lo.transform.localScale;
                if(lo.transform.localScale.x > 0.25)
                    lo.transform.localScale = new Vector3(v.x - 0.05,v.y - 0.05,v.z - 0.05);
                else lo.transform.localScale = new Vector3(v.x + 0.05,v.y + 0.05,v.z + 0.05);
            }
            else scaleActual = false;
        }
        if(demiosKey)
        {
            if(demios.transform.localScale.x != 0.1 && demios.transform.localScale.y != 0.1 && demios.transform.localScale.z != 0.1)
            {
                v = demios.transform.localScale;
                if(demios.transform.localScale.x > 0.1)
                    demios.transform.localScale = new Vector3(v.x - 0.01,v.y - 0.01,v.z - 0.01);
                else demios.transform.localScale = new Vector3(v.x + 0.01,v.y + 0.01,v.z + 0.01);
            }
            else scaleActual = false;
        }
        if(phobosKey)
        {
            if(phobos.transform.localScale.x != 0.1 && phobos.transform.localScale.y != 0.1 && phobos.transform.localScale.z != 0.1)
            {
                v = phobos.transform.localScale;
                if(phobos.transform.localScale.x > 0.1)
                    phobos.transform.localScale = new Vector3(v.x - 0.01,v.y - 0.01,v.z - 0.01);
                else phobos.transform.localScale = new Vector3(v.x + 0.01,v.y + 0.01,v.z + 0.01);
            }
            else scaleActual = false;
        }
        if(dionaKey)
        {
            if(diona.transform.localScale.x != 0.06 && diona.transform.localScale.y != 0.06 && diona.transform.localScale.z != 0.06)
            {
                v = diona.transform.localScale;
                if(diona.transform.localScale.x > 0.06)
                    diona.transform.localScale = new Vector3(v.x - 0.005,v.y - 0.005,v.z - 0.005);
                else diona.transform.localScale = new Vector3(v.x + 0.005,v.y + 0.005,v.z + 0.005);
            } 
            else scaleActual = false;
        }
        if(enceladusKey)
        {
            if(enceladus.transform.localScale.x != 0.05 && enceladus.transform.localScale.y != 0.05 && enceladus.transform.localScale.z != 0.05)
            {
                v = enceladus.transform.localScale;
                if(enceladus.transform.localScale.x > 0.05)
                    enceladus.transform.localScale = new Vector3(v.x - 0.005,v.y - 0.005,v.z - 0.005);
                else enceladus.transform.localScale = new Vector3(v.x + 0.005,v.y + 0.005,v.z + 0.005);
            } 
            else scaleActual = false;
        }
        if(mimasKey)
        {
            if(mimas.transform.localScale.x != 0.045 && mimas.transform.localScale.y != 0.045 && mimas.transform.localScale.z != 0.045)
            {
                v = mimas.transform.localScale;
                if(mimas.transform.localScale.x > 0.045)
                    mimas.transform.localScale = new Vector3(v.x - 0.015,v.y - 0.015,v.z - 0.015);
                else mimas.transform.localScale = new Vector3(v.x + 0.015,v.y + 0.015,v.z + 0.015);
            } 
            else scaleActual = false;
        }
        if(rheaKey)
        {
            if(rhea.transform.localScale.x != 0.09 && rhea.transform.localScale.y != 0.09 && rhea.transform.localScale.z != 0.09)
            {
                v = rhea.transform.localScale;
                if(rhea.transform.localScale.x > 0.09)
                    rhea.transform.localScale = new Vector3(v.x - 0.01,v.y - 0.01,v.z - 0.01);
                else rhea.transform.localScale = new Vector3(v.x + 0.01,v.y + 0.01,v.z + 0.01);
            } 
            else scaleActual = false;
        }
        if(tehysKey)
        {
            if(tehys.transform.localScale.x != 0.058 && tehys.transform.localScale.y != 0.058 && tehys.transform.localScale.z != 0.058)
            {
                v = tehys.transform.localScale;
                if(tehys.transform.localScale.x > 0.058)
                    tehys.transform.localScale = new Vector3(v.x - 0.011,v.y - 0.011,v.z - 0.011);
                else  tehys.transform.localScale = new Vector3(v.x + 0.011,v.y + 0.011,v.z + 0.011);
            } 
            else scaleActual = false;
        }
        if(titanKey)
        {
            if(titan.transform.localScale.x != 0.28 && titan.transform.localScale.y != 0.28 && titan.transform.localScale.z != 0.28)
            {
                v = titan.transform.localScale; 
                if(titan.transform.localScale.x > 0.28)
                    titan.transform.localScale = new Vector3(v.x - 0.02,v.y - 0.02,v.z - 0.02);
                else titan.transform.localScale = new Vector3(v.x + 0.02,v.y + 0.02,v.z + 0.02);
            }
            else scaleActual = false;
        }
    }
    else if(scaleBigger && !showingDetails && !follow)
    {
        if(sunKey)
        {
            if(sun.transform.localScale.x < 7 && sun.transform.localScale.y < 7 && sun.transform.localScale.z < 7)
            {
                v = sun.transform.localScale; 
                sun.transform.localScale = new Vector3(v.x + 0.5,v.y + 0.5,v.z + 0.5);
            }
        }
        if(earthKey)
        {
            if(earth.transform.localScale.x < 4 && earth.transform.localScale.y < 4 && earth.transform.localScale.z < 4)
            {
                v = earth.transform.localScale;
                earth.transform.localScale = new Vector3(v.x + 0.5,v.y + 0.5,v.z + 0.5);
            }
        }
        if(marsKey)
        {
            if(mars.transform.localScale.x < 3 && mars.transform.localScale.y < 3 && mars.transform.localScale.z < 3)
            {
                v = mars.transform.localScale;
                mars.transform.localScale = new Vector3(v.x + 0.5,v.y + 0.5,v.z + 0.5);
            }
        }
        if(mercuryKey)
        {
            if(mercury.transform.localScale.x < 5 && mercury.transform.localScale.y < 5 && mercury.transform.localScale.z < 5)
            {
                v = mercury.transform.localScale;
                mercury.transform.localScale = new Vector3(v.x + 0.5,v.y + 0.5,v.z + 0.5);
            }
        }
        if(plutoKey)
        {
            if(pluto.transform.localScale.x < 3 && pluto.transform.localScale.y < 3 && pluto.transform.localScale.z < 3)
            {
                v = pluto.transform.localScale;
                pluto.transform.localScale = new Vector3(v.x + 0.5,v.y + 0.5,v.z + 0.5);
            }
        } 
        if(jupiterKey)
        {
        	if(jupiter.transform.localScale.x < 6 && jupiter.transform.localScale.y < 6 && jupiter.transform.localScale.z < 6)
            {
                v = jupiter.transform.localScale;
                jupiter.transform.localScale = new Vector3(v.x + 0.5,v.y + 0.5,v.z + 0.5);
            }
        }
        if(neptuneKey)
        {
        	if(neptune.transform.localScale.x < 5 && neptune.transform.localScale.y < 5 && neptune.transform.localScale.z < 5)
            {
                v = neptune.transform.localScale; 
                neptune.transform.localScale = new Vector3(v.x + 0.5,v.y + 0.5,v.z + 0.5);
            }
        }
        if(saturnKey)
        {
        	if(saturn.transform.localScale.x < 5.5 && saturn.transform.localScale.y < 5.5 && saturn.transform.localScale.z < 5.5)
            {
                v = saturn.transform.localScale;
                saturn.transform.localScale = new Vector3(v.x + 0.5,v.y + 0.5,v.z + 0.5);
            }
        }
        if(uranusKey)
        {
        	if(uranus.transform.localScale.x < 5 && uranus.transform.localScale.y < 5 && uranus.transform.localScale.z < 5)
            {
                v = uranus.transform.localScale;
                uranus.transform.localScale = new Vector3(v.x + 0.5,v.y + 0.5,v.z + 0.5);
            }
        }
        if(venusKey)
        {
            if(venus.transform.localScale.x < 4 && venus.transform.localScale.y < 4 && venus.transform.localScale.z < 4)
            {
                v = venus.transform.localScale;
                venus.transform.localScale = new Vector3(v.x + 0.5,v.y + 0.5,v.z + 0.5);
            }
        }
        if(moonKey)
        {
            if(moon.transform.localScale.x < 0.5 && moon.transform.localScale.y < 0.5 && moon.transform.localScale.z < 0.5)
            {
                v = moon.transform.localScale;
                moon.transform.localScale = new Vector3(v.x + 0.1,v.y + 0.1,v.z + 0.1);
            }
        }
        if(calistoKey)
        {
            if(calisto.transform.localScale.x < 0.4 && calisto.transform.localScale.y < 0.4 && calisto.transform.localScale.z < 0.4)
            {
                v = calisto.transform.localScale;
                calisto.transform.localScale = new Vector3(v.x + 0.05,v.y + 0.05,v.z + 0.05);
            }
        }
        if(europaKey)
        {
            if(europa.transform.localScale.x < 0.25 && europa.transform.localScale.y < 0.25 && europa.transform.localScale.z < 0.25)
            {
                v = europa.transform.localScale;
                europa.transform.localScale = new Vector3(v.x + 0.05,v.y + 0.05,v.z + 0.05);
            }
        }
        if(ganymedeKey)
        {
            if(ganymede.transform.localScale.x < 0.35 && ganymede.transform.localScale.y < 0.35 && ganymede.transform.localScale.z < 0.35)
            {
                v = ganymede.transform.localScale;
                ganymede.transform.localScale = new Vector3(v.x + 0.05,v.y + 0.05,v.z + 0.05);
            }
        }
        if(loKey)
        {
            if(lo.transform.localScale.x < 0.35 && lo.transform.localScale.y < 0.35 && lo.transform.localScale.z < 0.35)
            {
                v = lo.transform.localScale;
                lo.transform.localScale = new Vector3(v.x + 0.05,v.y + 0.05,v.z + 0.05);
            }
        }
        if(demiosKey)
        {
            if(demios.transform.localScale.x < 0.12 && demios.transform.localScale.y < 0.12 && demios.transform.localScale.z < 0.12)
            {
                v = demios.transform.localScale;
                demios.transform.localScale = new Vector3(v.x + 0.01,v.y + 0.01,v.z + 0.01);
            }
        }
        if(phobosKey)
        {
            if(phobos.transform.localScale.x < 0.12 && phobos.transform.localScale.y < 0.12 && phobos.transform.localScale.z < 0.12)
            {
                v = phobos.transform.localScale;
                phobos.transform.localScale = new Vector3(v.x + 0.01,v.y + 0.01,v.z + 0.01);
            }
        }
        if(dionaKey)
        {
            if(diona.transform.localScale.x < 0.08 && diona.transform.localScale.y < 0.08 && diona.transform.localScale.z < 0.08)
            {
                v = diona.transform.localScale;
                diona.transform.localScale = new Vector3(v.x + 0.005,v.y + 0.005,v.z + 0.005);
            }
        }
        if(enceladusKey)
        {
            if(enceladus.transform.localScale.x < 0.07 && enceladus.transform.localScale.y < 0.07 && enceladus.transform.localScale.z < 0.07)
            {
                v = enceladus.transform.localScale;
                enceladus.transform.localScale = new Vector3(v.x + 0.005,v.y + 0.005,v.z + 0.005);
            }
        }
        if(mimasKey)
        {
            if(mimas.transform.localScale.x < 0.075 && mimas.transform.localScale.y < 0.075 && mimas.transform.localScale.z < 0.075)
            {
                v = mimas.transform.localScale;
                mimas.transform.localScale = new Vector3(v.x + 0.015,v.y + 0.015,v.z + 0.015);
            }
        }
        if(rheaKey)
        {
            if(rhea.transform.localScale.x < 0.11 && rhea.transform.localScale.y < 0.11 && rhea.transform.localScale.z < 0.11)
            {
                v = rhea.transform.localScale;
                rhea.transform.localScale = new Vector3(v.x + 0.01,v.y + 0.01,v.z + 0.01);
            }
        }
        if(tehysKey)
        {
            if(tehys.transform.localScale.x < 0.07 && tehys.transform.localScale.y < 0.07 && tehys.transform.localScale.z < 0.07)
            {
                v = tehys.transform.localScale;
                tehys.transform.localScale = new Vector3(v.x + 0.011,v.y + 0.011,v.z + 0.011);
            }
        }
        if(titanKey)
        {
            if(titan.transform.localScale.x < 0.5 && titan.transform.localScale.y < 0.5 && titan.transform.localScale.z < 0.5)
            {
                v = titan.transform.localScale;
                titan.transform.localScale = new Vector3(v.x + 0.02,v.y + 0.02,v.z + 0.02);
            }
        }
    }
}

function getSize() : Rect {
    var center : Vector3;
    var extends2 : Vector3;
    var cam : Camera = Camera.main;
    if(target.renderer != null)
    {
        center = target.renderer.bounds.center;
        extends2 = target.renderer.bounds.extents;
    }
    else
    {
        center = target.transform.position;
        extends2 = new Vector3(15.0, 15.0, 0);
    }
    var vec : Vector3[] = new Vector3[8];
    vec[0] = cam.WorldToScreenPoint(new Vector3(center.x - extends2.x, center.y + extends2.y, center.z + extends2.z));
    vec[1] = cam.WorldToScreenPoint(new Vector3(center.x + extends2.x, center.y + extends2.y, center.z + extends2.z));
    vec[2] = cam.WorldToScreenPoint(new Vector3(center.x - extends2.x, center.y - extends2.y, center.z + extends2.z));
    vec[3] = cam.WorldToScreenPoint(new Vector3(center.x + extends2.x, center.y - extends2.y, center.z + extends2.z));
    
    vec[4] = cam.WorldToScreenPoint(new Vector3(center.x - extends2.x, center.y + extends2.y, center.z-extends2.z));
    vec[5] = cam.WorldToScreenPoint(new Vector3(center.x + extends2.x, center.y + extends2.y, center.z-extends2.z));
    vec[6] = cam.WorldToScreenPoint(new Vector3(center.x - extends2.x, center.y - extends2.y, center.z-extends2.z));
    vec[7] = cam.WorldToScreenPoint(new Vector3(center.x + extends2.x, center.y - extends2.y, center.z-extends2.z));
    
    var xMinf : float;
    var xMaxf : float;
    var yMinf : float;
    var yMaxf : float;
    xMaxf = yMaxf = 0;
    xMinf = yMinf = 10000;
    
    for (var i : int= 0; i < 8; i++)
    {
        if(vec[i].x < xMinf) 
            xMinf = vec[i].x;
        if(vec[i].y < yMinf)
            yMinf = vec[i].y;
        if(vec[i].x > xMaxf)
            xMaxf = vec[i].x;
        if(vec[i].y > yMaxf)
            yMaxf = vec[i].y;
    }
    var boxRect : Rect = new Rect(xMinf - (xMaxf-xMinf)*0.75, (Screen.height-yMinf-(yMaxf-yMinf)*0.75), (xMaxf-xMinf)*1.5, (yMaxf-yMinf)*1.5);
    
    return boxRect;
}

static function ClampAngle (angle : float, min : float, max : float) {
	if (angle < -360)
		angle += 360;
	if (angle > 360)
		angle -= 360;
	return Mathf.Clamp (angle, min, max);
}